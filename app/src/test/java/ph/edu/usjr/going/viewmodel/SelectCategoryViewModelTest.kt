package ph.edu.usjr.going.viewmodel

import org.junit.Test

import com.google.common.truth.Truth.assertThat
import org.junit.Before
import ph.edu.usjr.going.BaseTest

class SelectCategoryViewModelTest : BaseTest() {
    private lateinit var mCategoryVm : SelectCategoryViewModel
    @Before
    fun setUp(){
        mCategoryVm = SelectCategoryViewModel()
        assertThat(mCategoryVm.mode).isNotNull()
        assertThat(mCategoryVm.categoryType).isNotNull()
        assertThat(mCategoryVm.mode.value).isNull()
        assertThat(mCategoryVm.categoryType.value).isEqualTo(0)
    }
    @Test
    fun should_return_when_values_are_correct() {
        //Assign
        mCategoryVm.mode.value = 5
        mCategoryVm.categoryType.value = 0

        //Act
        val result = mCategoryVm.isCategoryValid()

        //Assert
        assertThat(result).isTrue()
    }
    @Test
    fun should_return_false_when_mode_is_incorrect() {
        //Assign
        mCategoryVm.mode.value = 10
        mCategoryVm.categoryType.value = 0

        //Act
        val result = mCategoryVm.isCategoryValid()

        //Assert
        assertThat(result).isFalse()
    }
    @Test
    fun should_return_false_when_type_is_incorrect() {
        //Assign
        mCategoryVm.mode.value = 5
        mCategoryVm.categoryType.value = 10

        //Act
        val result = mCategoryVm.isCategoryValid()

        //Assert
        assertThat(result).isFalse()
    }
}