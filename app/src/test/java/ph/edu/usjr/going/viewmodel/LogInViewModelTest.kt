package ph.edu.usjr.going.viewmodel

import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import ph.edu.usjr.going.BaseTest

class LogInViewModelTest : BaseTest() {
    private lateinit var mLoginVM: LogInViewModel

    @Before
    fun setup() {
        mLoginVM = LogInViewModel()
        assertThat(mLoginVM.email).isNotNull()
        assertThat(mLoginVM.password).isNotNull()
        assertThat(mLoginVM.email.value).isNull()
        assertThat(mLoginVM.password.value).isNull()
    }

    @Test
    fun should_return_true_when_emailAndPasswordAreValid() {
        // Assign
        mLoginVM.email.value = "test@gmail.com"
        mLoginVM.password.value = "sad"

        // Act
        val result = mLoginVM.isValidLogIn()

        // Assert
        assertThat(result).isTrue()
    }

    @Test
    fun should_return_false_when_emailIsNullOrEmpty() {
        // Assign
        mLoginVM.email.value = null
        mLoginVM.password.value = "valid123"

        // Act
        var result = mLoginVM.isValidLogIn()

        // Assert
        assertThat(result).isFalse()

        // Assign
        mLoginVM.email.value = ""
        mLoginVM.password.value = "valid123"

        // Act
        result = mLoginVM.isValidLogIn()

        // Assert
        assertThat(result).isFalse()
    }

    @Test
    fun should_return_false_when_emailIsNotValid() {
        // Assign
        mLoginVM.email.value = "invalid-email"
        mLoginVM.password.value = "valid123"

        // Act
        val result = mLoginVM.isValidLogIn()

        // Assert
        assertThat(result).isFalse()
    }

    @Test
    fun should_return_false_when_passwordIsNullOrEmpty() {
        // Assign
        mLoginVM.email.value = "tester@going.com"
        mLoginVM.password.value = null

        // Act
        var result = mLoginVM.isValidLogIn()

        // Assert
        assertThat(result).isFalse()

        // Assign
        mLoginVM.email.value = "test2@going.com"
        mLoginVM.password.value = ""

        // Act
        result = mLoginVM.isValidLogIn()

        // Assert
        assertThat(result).isFalse()
    }
}
