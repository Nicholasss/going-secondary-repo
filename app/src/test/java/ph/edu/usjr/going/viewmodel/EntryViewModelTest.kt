package ph.edu.usjr.going.viewmodel

import org.junit.Test

import org.junit.Before
import ph.edu.usjr.going.BaseTest
import com.google.common.truth.Truth.assertThat
import com.google.firebase.Timestamp
import ph.edu.usjr.going.model.Category
import ph.edu.usjr.going.model.Location
import java.util.*

class EntryViewModelTest : BaseTest() {

    private lateinit var mEntryVm : EntryViewModel

    @Before
    fun setUp(){
        mEntryVm = EntryViewModel()
        assertThat(mEntryVm.title).isNotNull()
        assertThat(mEntryVm.cost).isNotNull()
        assertThat(mEntryVm.notes).isNotNull()
        assertThat(mEntryVm.destination).isNotNull()
        assertThat(mEntryVm.tag).isNotNull()
        assertThat(mEntryVm.startTime).isNotNull()
        assertThat(mEntryVm.startDateText).isNotNull()
        assertThat(mEntryVm.destinationLocation).isNotNull()
        assertThat(mEntryVm.category).isNotNull()
        assertThat(mEntryVm.startDate).isNotNull()
        assertThat(mEntryVm.finishedDate).isNotNull()
        assertThat(mEntryVm.title.value).isNull()
        assertThat(mEntryVm.cost.value).isNull()
        assertThat(mEntryVm.notes.value).isNull()
        assertThat(mEntryVm.destination.value).isNull()
        assertThat(mEntryVm.tag.value).isNull()
        assertThat(mEntryVm.startTime.value).isNull()
        assertThat(mEntryVm.startDateText.value).isNull()
        assertThat(mEntryVm.destinationLocation.value).isNull()
        assertThat(mEntryVm.category.value).isNull()
        assertThat(mEntryVm.startDate.value).isNull()
        assertThat(mEntryVm.finishedDate.value).isNull()
    }

    @Test
    fun return_true_when_values_of_entry_is_correct() {
        //Assign
        mEntryVm.title.value = "beach, yeah!"
        mEntryVm.cost.value = "500.00"
        mEntryVm.notes.value = "many nekkid people"
        mEntryVm.destination.value = "California Nekkid Beach"
        mEntryVm.tag.value = "wow,amazing,really,lot,of,nekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0,10.0,"California Nekkid Beach")
        mEntryVm.category.value = Category(0,5)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isTrue()
    }

    @Test
    fun return_false_when_values_of_title_is_incorrect() {
        //Assign
        mEntryVm.title.value = ""
        mEntryVm.cost.value = "500.00"
        mEntryVm.notes.value = "many nekkid people"
        mEntryVm.destination.value = "California Nekkid Beach"
        mEntryVm.tag.value = "wow,amazing,really,lot,of,nekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0,10.0,"California Nekkid Beach")
        mEntryVm.category.value = Category(0,5)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun return_false_when_values_of_cost_is_incorrect() {
        //Assign
        mEntryVm.title.value = "beach, yeah!"
        mEntryVm.cost.value = "sadadas.00"
        mEntryVm.notes.value = "many nekkid people"
        mEntryVm.destination.value = "California Nekkid Beach"
        mEntryVm.tag.value = "wow,amazing,really,lot,of,nekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0,10.0,"California Nekkid Beach")
        mEntryVm.category.value = Category(0,5)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun return_true_when_values_of_notes_is_empty() {
        //Assign
        mEntryVm.title.value = "beach, yeah!"
        mEntryVm.cost.value = "500.00"
        mEntryVm.notes.value = ""
        mEntryVm.destination.value = "California Nekkid Beach"
        mEntryVm.tag.value = "wow,amazing,really,lot,of,nekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0, 10.0, "California Nekkid Beach")
        mEntryVm.category.value = Category(0, 5)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000, 0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000, 0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isTrue()
    }

    @Test
    fun return_false_when_values_of_destination_is_incorrect() {
        //Assign
        mEntryVm.title.value = "beach, yeah!"
        mEntryVm.cost.value = "500.00"
        mEntryVm.notes.value = "many nekkid people"
        mEntryVm.destination.value = "California Nekkid Beacah"
        mEntryVm.tag.value = "wow,amazing,really,lot,of,nekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0,10.0,"California Nekkid Beach")
        mEntryVm.category.value = Category(0,5)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun return_false_when_values_of_tags_is_incorrect() {
        //Assign
        mEntryVm.title.value = "beach, yeah!"
        mEntryVm.cost.value = "500.00"
        mEntryVm.notes.value = "many nekkid people"
        mEntryVm.destination.value = "California Nekkid Beach"
        mEntryVm.tag.value = "wow ,amazing1,real 12ly,lot ,of ,n ekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0,10.0,"California Nekkid Beach")
        mEntryVm.category.value = Category(0,5)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun return_false_when_values_of_Category_type_is_incorrect() {
        //Assign
        mEntryVm.title.value = "beach, yeah!"
        mEntryVm.cost.value = "500.00"
        mEntryVm.notes.value = "many nekkid people"
        mEntryVm.destination.value = "California Nekkid Beach"
        mEntryVm.tag.value = "wow,amazing,really,lot,of,nekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0,10.0,"California Nekkid Beach")
        mEntryVm.category.value = Category(3,5)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun return_false_when_values_of_category_mode_is_incorrect() {
        //Assign
        mEntryVm.title.value = "beach, yeah!"
        mEntryVm.cost.value = "500.00"
        mEntryVm.notes.value = "many nekkid people"
        mEntryVm.destination.value = "California Nekkid Beach"
        mEntryVm.tag.value = "wow,amazing,really,lot,of,nekkid"
        mEntryVm.startTime.value = "5:00PM"
        mEntryVm.startDateText.value = "2018-09-10"
        mEntryVm.destinationLocation.value = Location(50.0,10.0,"California Nekkid Beach")
        mEntryVm.category.value = Category(0,8)
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar

        //Act
        val result = mEntryVm.isValidEntry()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun should_return_true_when_start_date_is_correct() {
        //Assign
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2500000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar

        //Act
        val result = mEntryVm.isValidStartDate()

        //Assert
        assertThat(result).isTrue()
    }

    @Test
    fun should_return_false_when_start_date_is_incorrect() {
        //Assign
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(1000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar

        //Act
        val result = mEntryVm.isValidStartDate()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun should_return_false_when_start_date_is_greater_than_end_date() {
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.time = Timestamp(2900000000,0).toDate()
        mEntryVm.startDate.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.time = Timestamp(2800000000,0).toDate()
        mEntryVm.finishedDate.value = endDateCalendar


        //Act
        val result = mEntryVm.isValidStartDate()

        //Assert
        assertThat(result).isFalse()
    }
}