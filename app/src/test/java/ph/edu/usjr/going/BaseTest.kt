package ph.edu.usjr.going

import android.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Rule

open class BaseTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
}
