package ph.edu.usjr.going.viewmodel

import org.junit.Test
import com.google.common.truth.Truth.assertThat
import com.google.firebase.Timestamp
import org.junit.Before
import ph.edu.usjr.going.BaseTest
import ph.edu.usjr.going.model.Category
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Location
import java.util.*
import kotlin.collections.ArrayList

class ItineraryViewModelTest : BaseTest(){
    private lateinit var mItineraryVm : ItineraryViewModel
    @Before
    fun setUp(){
        mItineraryVm = ItineraryViewModel()
        assertThat(mItineraryVm.totalCost).isNotNull()
        assertThat(mItineraryVm.travelName).isNotNull()
        assertThat(mItineraryVm.travelerCount).isNotNull()
        assertThat(mItineraryVm.origin).isNotNull()
        assertThat(mItineraryVm.destination).isNotNull()
        assertThat(mItineraryVm.startDate).isNotNull()
        assertThat(mItineraryVm.endDate).isNotNull()
        assertThat(mItineraryVm.startDateCalendar).isNotNull()
        assertThat(mItineraryVm.endDateCalendar).isNotNull()
        assertThat(mItineraryVm.entriesCollection).isNotNull()
        assertThat(mItineraryVm.originLoc).isNotNull()
        assertThat(mItineraryVm.destinationLoc).isNotNull()
        assertThat(mItineraryVm.totalCost.value).isNotNull()
        assertThat(mItineraryVm.travelName.value).isNull()
        assertThat(mItineraryVm.travelerCount.value).isNull()
        assertThat(mItineraryVm.origin.value).isNull()
        assertThat(mItineraryVm.destination.value).isNull()
        assertThat(mItineraryVm.startDate.value).isNull()
        assertThat(mItineraryVm.endDate.value).isNull()
        assertThat(mItineraryVm.startDateCalendar.value).isNull()
        assertThat(mItineraryVm.endDateCalendar.value).isNull()
        assertThat(mItineraryVm.entriesCollection.value).isNotNull()
        assertThat(mItineraryVm.originLoc.value).isNull()
        assertThat(mItineraryVm.destinationLoc.value).isNull()
    }
    @Test
    fun should_return_true_when_inputs_are_correct() {
        //Assign
        mItineraryVm.totalCost.value = "50"
        mItineraryVm.travelName.value = "test"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isTrue()

    }

    @Test
    fun should_return_false_when_total_cost_is_invalid() {
        //Assign
        mItineraryVm.totalCost.value = "just a text"
        mItineraryVm.travelName.value = "test"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_travel_title_is_invalid() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = ""
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }

    @Test
    fun should_return_false_when_traveler_count_is_invalid() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "test"
        mItineraryVm.travelerCount.value = "a text"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_origin_text_is_invalid() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = ""
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_destination_text_is_invalid() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = ""
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_startDate_text_is_invalid() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = ""
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_true_when_endDate_text_is_invalid() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = ""
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isTrue()

    }

    @Test
    fun should_return_false_when_startDate_is_null() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_startDate_is_less_than_current_time() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2016,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_startDate_is_greater_than_endDate() {
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2025,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_true_when_endDate_is_null(){
        //Assign
        mItineraryVm.totalCost.value = "20"
        mItineraryVm.travelName.value = "just a text"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isTrue()

    }
    @Test
    fun should_return_false_when_originLocation_is_null() {
        //Assign
        mItineraryVm.totalCost.value = "50"
        mItineraryVm.travelName.value = "test"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.destinationLoc.value = Location(0.0,1.0,"secondSample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_destinationLocation_is_null() {
        //Assign
        mItineraryVm.totalCost.value = "50"
        mItineraryVm.travelName.value = "test"
        mItineraryVm.travelerCount.value = "20"
        mItineraryVm.origin.value = "Philippines"
        mItineraryVm.destination.value = "Philadelphia"
        mItineraryVm.startDate.value = "2020-09-11"
        mItineraryVm.endDate.value = "2021-09-11"
        val startDateCalendar = Calendar.getInstance()
        startDateCalendar.set(2020,8,11)
        mItineraryVm.startDateCalendar.value = startDateCalendar
        val endDateCalendar = Calendar.getInstance()
        endDateCalendar.set(2021,8,11)
        mItineraryVm.endDateCalendar.value = endDateCalendar
        val entryList = ArrayList<Entry>()
        entryList.add(Entry())
        mItineraryVm.entriesCollection.value = entryList
        mItineraryVm.originLoc.value = Location(0.0,1.0,"sample")

        //Act
        val result = mItineraryVm.isItineraryValid()

        //Assert
        assertThat(result).isFalse()

    }
    @Test
    fun should_return_false_when_entries_is_null(){
        //Assign
        mItineraryVm.entriesCollection.value = null

        //Act
        val result = mItineraryVm.isEntriesListValid()

        //Assert
        assertThat(result).isFalse()
    }

    @Test
    fun should_return_false_when_entries_is_empty(){
        //Assign
        mItineraryVm.entriesCollection.value = arrayListOf()

        //Act
        val result = mItineraryVm.isEntriesListValid()

        //Assert
        assertThat(result).isFalse()

    }

    @Test
    fun should_return_true_when_add_calculation_is_correct(){
        //Assign
        mItineraryVm.totalCost.value = "0.0"
        val entriesList : ArrayList<Entry> = arrayListOf()
        entriesList.add(Entry("sample1",50.0, Timestamp(0,0),null,"lol",Location(),"sample",
            Category(),null,null))
        entriesList.add(Entry("sample1",25.0, Timestamp(0,0),null,"lol",Location(),"sample",
            Category(),null,null))
        mItineraryVm.entriesCollection.value = entriesList

        //Act
        val result = mItineraryVm.addToTotalCost()

        assertThat(result).isEqualTo(75.0)
    }

    @Test
    fun should_return_true_when_subtract_calculation_is_correct(){
        //Assign
        mItineraryVm.totalCost.value = "0.0"
        val entriesList : ArrayList<Entry> = arrayListOf()
        entriesList.add(Entry("sample1",50.0, Timestamp(0,0),null,"lol",Location(),"sample",
            Category(),null,null))
        entriesList.add(Entry("sample1",25.0, Timestamp(0,0),null,"lol",Location(),"sample",
            Category(),null,null))
        mItineraryVm.entriesCollection.value = entriesList
        mItineraryVm.totalCost.value = mItineraryVm.addToTotalCost().toString()

        //Act
        val result = mItineraryVm.subtractTotalCostFromEntryPosition(0)

        //Assert
        assertThat(result).isTrue()
        assertThat(mItineraryVm.totalCost.value?.toDouble()).isEqualTo(25.0)
    }

    @Test
    fun should_return_false_when_subtract_calculation_position_is_out_of_bounds(){
        //Assign
        mItineraryVm.totalCost.value = "0.0"
        val entriesList : ArrayList<Entry> = arrayListOf()
        entriesList.add(Entry("sample1",50.0, Timestamp(0,0),null,"lol",Location(),"sample",
            Category(),null,null))
        entriesList.add(Entry("sample1",25.0, Timestamp(0,0),null,"lol",Location(),"sample",
            Category(),null,null))
        mItineraryVm.entriesCollection.value = entriesList
        mItineraryVm.totalCost.value = mItineraryVm.addToTotalCost().toString()

        //Act
        val result = mItineraryVm.subtractTotalCostFromEntryPosition(2)

        //Assert
        assertThat(result).isFalse()
        assertThat(mItineraryVm.totalCost.value?.toDouble()).isEqualTo(75.0)
    }

    @Test fun should_return_true_when_start_date_is_correct(){
        //Assign
        val entriesList : ArrayList<Entry> = arrayListOf()
        entriesList.add(Entry("sample1",50.0, Timestamp(2000,0),null,"lol",Location(),"sample",
            Category(),null,null))
        entriesList.add(Entry("sample1",25.0, Timestamp(1500,0),null,"lol",Location(),"sample",
            Category(),null,null))
        entriesList.add(Entry("sample1",25.0, Timestamp(1750,0),null,"lol",Location(),"sample",
            Category(),null,null))
        mItineraryVm.entriesCollection.value = entriesList

        //Act
        mItineraryVm.setStartDateAndEndDate()

        assertThat(mItineraryVm.startDateCalendar.value?.timeInMillis).isEqualTo(1500000)
    }

    @Test
    fun should_return_true_when_end_date_is_correct(){
        //Assign
        val entriesList : ArrayList<Entry> = arrayListOf()
        entriesList.add(Entry("sample1",50.0, Timestamp(2000,0),null,"lol",Location(),"sample",
            Category(),null,null))
        entriesList.add(Entry("sample1",25.0, Timestamp(1500,0),null,"lol",Location(),"sample",
            Category(),null,null))
        entriesList.add(Entry("sample1",25.0, Timestamp(1750,0),null,"lol",Location(),"sample",
            Category(),null,null))
        mItineraryVm.entriesCollection.value = entriesList

        //Act
        mItineraryVm.setStartDateAndEndDate()

        //Assert
        assertThat(mItineraryVm.endDateCalendar.value?.timeInMillis).isEqualTo(2000000)
    }

    @Test
    fun should_return_true_when_values_are_according_to_input(){
        //Assign
        val id = "sample1"
        val title = "title"
        val count = 20
        val origin = Location(10.0,20.0,"lol")
        val destination = Location(20.0,50.0,"man")

        //Act
        mItineraryVm.inputValues(id,title,count,origin,destination)

        assertThat(mItineraryVm.id.value).isEqualTo(id)
        assertThat(mItineraryVm.travelName.value).isEqualTo(title)
        assertThat(mItineraryVm.travelerCount.value).isEqualTo(count.toString())
        assertThat(mItineraryVm.originLoc.value).isEqualTo(origin)
        assertThat(mItineraryVm.origin.value).isEqualTo(origin.address)
        assertThat(mItineraryVm.destinationLoc.value).isEqualTo(destination)
        assertThat(mItineraryVm.destination.value).isEqualTo(destination.address)
    }

    @Test
    fun should_return_false_when_id_does_not_match(){
        //Assign
        val id = "sample1"
        val title = "title"
        val count = 20
        val origin = Location(10.0,20.0,"lol")
        val destination = Location(20.0,50.0,"man")

        //Act
        mItineraryVm.inputValues(id,title,count,origin,destination)

        assertThat(mItineraryVm.id.value).isNotEqualTo("sample2")
        assertThat(mItineraryVm.travelName.value).isEqualTo(title)
        assertThat(mItineraryVm.travelerCount.value).isEqualTo(count.toString())
        assertThat(mItineraryVm.originLoc.value).isEqualTo(origin)
        assertThat(mItineraryVm.origin.value).isEqualTo(origin.address)
        assertThat(mItineraryVm.destinationLoc.value).isEqualTo(destination)
        assertThat(mItineraryVm.destination.value).isEqualTo(destination.address)
    }

    @Test
    fun should_return_false_when_title_does_not_match(){
        //Assign
        val id = "sample1"
        val title = "title2"
        val count = 20
        val origin = Location(10.0,20.0,"lol")
        val destination = Location(20.0,50.0,"man")

        //Act
        mItineraryVm.inputValues(id,title,count,origin,destination)

        assertThat(mItineraryVm.id.value).isEqualTo(id)
        assertThat(mItineraryVm.travelName.value).isNotEqualTo("title4")
        assertThat(mItineraryVm.travelerCount.value).isEqualTo(count.toString())
        assertThat(mItineraryVm.originLoc.value).isEqualTo(origin)
        assertThat(mItineraryVm.origin.value).isEqualTo(origin.address)
        assertThat(mItineraryVm.destinationLoc.value).isEqualTo(destination)
        assertThat(mItineraryVm.destination.value).isEqualTo(destination.address)
    }

    @Test
    fun should_return_false_when_traveler_count_does_not_match(){
        //Assign
        val id = "sample1"
        val title = "title"
        val count = 20
        val origin = Location(10.0,20.0,"lol")
        val destination = Location(20.0,50.0,"man")

        //Act
        mItineraryVm.inputValues(id,title,count,origin,destination)

        assertThat(mItineraryVm.id.value).isEqualTo(id)
        assertThat(mItineraryVm.travelName.value).isEqualTo(title)
        assertThat(mItineraryVm.travelerCount.value).isNotEqualTo(count)
        assertThat(mItineraryVm.originLoc.value).isEqualTo(origin)
        assertThat(mItineraryVm.origin.value).isEqualTo(origin.address)
        assertThat(mItineraryVm.destinationLoc.value).isEqualTo(destination)
        assertThat(mItineraryVm.destination.value).isEqualTo(destination.address)
    }
    @Test
    fun should_return_false_when_origin_does_not_match(){
        //Assign
        val id = "sample1"
        val title = "title"
        val count = 20
        val origin = Location(10.0,20.0,"lol")
        val destination = Location(20.0,50.0,"man")

        //Act
        mItineraryVm.inputValues(id,title,count,origin,destination)

        assertThat(mItineraryVm.id.value).isEqualTo(id)
        assertThat(mItineraryVm.travelName.value).isEqualTo(title)
        assertThat(mItineraryVm.travelerCount.value).isEqualTo(count.toString())
        assertThat(mItineraryVm.originLoc.value).isNotEqualTo(destination)
        assertThat(mItineraryVm.origin.value).isNotEqualTo(destination.address)
        assertThat(mItineraryVm.destinationLoc.value).isEqualTo(destination)
        assertThat(mItineraryVm.destination.value).isEqualTo(destination.address)
    }

    @Test
    fun should_return_false_when_destination_does_not_match(){
        //Assign
        val id = "sample1"
        val title = "title"
        val count = 20
        val origin = Location(10.0,20.0,"lol")
        val destination = Location(20.0,50.0,"man")

        //Act
        mItineraryVm.inputValues(id,title,count,origin,destination)

        assertThat(mItineraryVm.id.value).isEqualTo(id)
        assertThat(mItineraryVm.travelName.value).isEqualTo(title)
        assertThat(mItineraryVm.travelerCount.value).isEqualTo(count.toString())
        assertThat(mItineraryVm.originLoc.value).isEqualTo(origin)
        assertThat(mItineraryVm.origin.value).isEqualTo(origin.address)
        assertThat(mItineraryVm.destinationLoc.value).isNotEqualTo(origin)
        assertThat(mItineraryVm.destination.value).isNotEqualTo(origin.address)
    }
}