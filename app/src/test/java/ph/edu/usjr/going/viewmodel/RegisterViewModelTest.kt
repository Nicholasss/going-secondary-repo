package ph.edu.usjr.going.viewmodel

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.Before
import ph.edu.usjr.going.BaseTest
import java.util.*

class RegisterViewModelTest : BaseTest(){

    private lateinit var mRegisterVm : RegisterViewModel

    @Before
    fun setUp(){
        mRegisterVm = RegisterViewModel()
        assertThat(mRegisterVm.email).isNotNull()
        assertThat(mRegisterVm.password).isNotNull()
        assertThat(mRegisterVm.firstName).isNotNull()
        assertThat(mRegisterVm.lastName).isNotNull()
        assertThat(mRegisterVm.birthday).isNotNull()
        assertThat(mRegisterVm.email.value).isNull()
        assertThat(mRegisterVm.password.value).isNull()
        assertThat(mRegisterVm.firstName.value).isNull()
        assertThat(mRegisterVm.lastName.value).isNull()
        assertThat(mRegisterVm.birthday.value).isNull()
         }


    @Test
    fun should_return_true_when_inputs_are_correct() {
        //assign
        mRegisterVm.birthDateCalendar.set(Calendar.DAY_OF_MONTH,4)
        mRegisterVm.birthDateCalendar.set(Calendar.MONTH,11)
        mRegisterVm.birthDateCalendar.set(Calendar.YEAR,1998)
        mRegisterVm.email.value = "test@gmail.com"
        mRegisterVm.password.value = "testaccount321"
        mRegisterVm.firstName.value = "James"
        mRegisterVm.lastName.value = "Charles"
        mRegisterVm.birthday.value = "4-11-1998"
        val result = mRegisterVm.checkInput()
        assertThat(result).isTrue()
    }


    @Test
    fun should_return_false_when_year_is_incorrect() {
        //assign
        mRegisterVm.birthDateCalendar.set(Calendar.DAY_OF_MONTH,2)
        mRegisterVm.birthDateCalendar.set(Calendar.MONTH,11)
        mRegisterVm.birthDateCalendar.set(Calendar.YEAR,1200)
        mRegisterVm.email.value = "test@gmail.com"
        mRegisterVm.password.value = "testaccount321"
        mRegisterVm.firstName.value = "James"
        mRegisterVm.lastName.value = "Charles"
        mRegisterVm.birthday.value = "4-11-1998"

        //act
        val result = mRegisterVm.checkInput()

        //assert
        assertThat(result).isFalse()
    }

    @Test
    fun should_return_false_when_email_is_incorrect() {
        //assign
        mRegisterVm.birthDateCalendar.set(Calendar.DAY_OF_MONTH,2)
        mRegisterVm.birthDateCalendar.set(Calendar.MONTH,11)
        mRegisterVm.birthDateCalendar.set(Calendar.YEAR,1998)
        mRegisterVm.email.value = "test.com"
        mRegisterVm.password.value = "testaccount321"
        mRegisterVm.firstName.value = "James"
        mRegisterVm.lastName.value = "Charles"
        mRegisterVm.birthday.value = "4-11-1998"

        //act
        val result = mRegisterVm.checkInput()

        //assert
        assertThat(result).isFalse()
    }

    @Test
    fun should_return_false_when_password_is_not_viable() {
        //assign
        mRegisterVm.birthDateCalendar.set(Calendar.DAY_OF_MONTH,2)
        mRegisterVm.birthDateCalendar.set(Calendar.MONTH,11)
        mRegisterVm.birthDateCalendar.set(Calendar.YEAR,1998)
        mRegisterVm.email.value = "test@gmail.com"
        mRegisterVm.password.value = "1234"
        mRegisterVm.firstName.value = "James"
        mRegisterVm.lastName.value = "Charles"
        mRegisterVm.birthday.value = "4-11-1998"

        //act
        val result = mRegisterVm.checkInput()

        //assert
        assertThat(result).isFalse()
    }
    @Test
    fun should_return_false_when_name_is_incorrect_format() {
        //assign

        mRegisterVm.birthDateCalendar.set(Calendar.DAY_OF_MONTH,2)
        mRegisterVm.birthDateCalendar.set(Calendar.MONTH,11)
        mRegisterVm.birthDateCalendar.set(Calendar.YEAR,1998)
        mRegisterVm.email.value = "test@gmail.com"
        mRegisterVm.password.value = "testaccount321"
        mRegisterVm.firstName.value = ""
        mRegisterVm.lastName.value = ""
        mRegisterVm.birthday.value = "4-11-1998"

        //act
        val result = mRegisterVm.checkInput()

        //assert
        assertThat(result).isFalse()
    }
    @Test
    fun should_return_false_when_birthday_text_is_blank() {
        //assign

        mRegisterVm.birthDateCalendar.set(Calendar.DAY_OF_MONTH,2)
        mRegisterVm.birthDateCalendar.set(Calendar.MONTH,11)
        mRegisterVm.birthDateCalendar.set(Calendar.YEAR,1998)
        mRegisterVm.email.value = "test@gmail.com"
        mRegisterVm.password.value = "testaccount"
        mRegisterVm.firstName.value = "James"
        mRegisterVm.lastName.value = "Charles"
        mRegisterVm.birthday.value = ""

        //act
        val result = mRegisterVm.checkInput()

        //assert
        assertThat(result).isFalse()
    }

}