package ph.edu.usjr.going.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Bindable
import ph.edu.usjr.going.model.Category
import ph.edu.usjr.going.model.Location
import ph.edu.usjr.going.model.Mode
import java.lang.NumberFormatException
import java.util.*
import java.util.regex.Pattern

class EntryViewModel : ViewModel() {
    @Bindable
    val title = MutableLiveData<String>()
    @Bindable
    val cost = MutableLiveData<String>()

    init {
        cost.value = "0.0"
    }

    @Bindable
    val notes = MutableLiveData<String>()
    @Bindable
    val origin = MutableLiveData<String>()
    @Bindable
    val destination = MutableLiveData<String>()
    @Bindable
    val tag = MutableLiveData<String>()
    @Bindable
    val startTime = MutableLiveData<String>()
    @Bindable
    val startDateText = MutableLiveData<String>()
    @Bindable
    val transportationName = MutableLiveData<String>()
    @Bindable
    val modeName = MutableLiveData<String>()

    var originLocation = MutableLiveData<Location>()
    var destinationLocation = MutableLiveData<Location>()
    val category = MutableLiveData<Category>()
    val startDate = MutableLiveData<Calendar>()
    val finishedDate = MutableLiveData<Calendar>()
    val day = MutableLiveData<String>()
    val transportation = MutableLiveData<Mode>()
    val mode = MutableLiveData<Mode>()
    val nights = MutableLiveData<String>()
    val transportationCost = MutableLiveData<String>()
    val activityCost = MutableLiveData<String>()
    val transportationId = MutableLiveData<String>()
    val id = MutableLiveData<String>()
    var isCheckedIn = MutableLiveData<Boolean>()
    init {
        isCheckedIn.value = false
    }
    /**
     * this has no use for now.
     */
//    private fun isTitleValid(): Boolean {
//        val isValid = true
//        val title = title.value
//        if (title.isNullOrBlank()) {
//            return false
//        }
//        return isValid
//    }
    fun isTransportationCostValid(): Boolean{
        val isValid = true
        val cost = transportationCost.value
        if (cost.isNullOrBlank()) {
            return false
        }
        try {
            cost.toDouble()
        } catch (e: NumberFormatException) {
            return false
        }
        return isValid
    }
    fun isNightsValid():Boolean{
        val isValid = true
        val nights = nights.value
        if (nights.isNullOrBlank()) {
            return false
        }
        try {
            nights.toInt()
        } catch (e: NumberFormatException) {
            return false
        }
        return isValid
    }
    fun isActivityCostValid(): Boolean{
        val isValid = true
        val cost = activityCost.value
        if (cost.isNullOrBlank()) {
            return false
        }
        try {
            cost.toDouble()
        } catch (e: NumberFormatException) {
            return false
        }
        return isValid
    }

    fun isCostValid(): Boolean {
        val isValid = true
        val cost = cost.value
        if (cost.isNullOrBlank()) {
            return false
        }
        try {
            cost.toDouble()
        } catch (e: NumberFormatException) {
            return false
        }
        return isValid
    }

    fun isOriginValid(): Boolean {
        val isValid = true
        val originText = origin.value
        val origin = originLocation.value
        if (originText.isNullOrBlank()) {
            return false
        }
        if (origin == null) {
            return false
        }
        if (originText != origin.address) {
            return false
        }
        return isValid
    }

    fun isDestinationValid(): Boolean {
        val isValid = true
        val destinationText = destination.value
        val destination = destinationLocation.value
        if (destinationText.isNullOrBlank()) {
            return false
        }
        if (destination == null) {
            return false
        }
        if (destinationText != destination.address) {
            return false
        }
        return isValid
    }

    private fun isCategoryValid(): Boolean {
        val isValid = true
        val category = category.value ?: return false
        if (category.categoryType < 0 || category.categoryType > 2) {
            return false
        }
        if (category.mode.isBlank()) {
            return false
        }
        return isValid
    }

    fun isModeValid(): Boolean {
        val isValid = true
        mode.value ?: return false
        return isValid
    }

    fun isTransportationValid(): Boolean {
        val isValid = true
        transportation.value ?: return false
        return isValid
    }

    fun isValidEntry(): Boolean {
        val isValid = true
        if (!isOriginValid()) {
            return false
        }
        if (!isDestinationValid()) {
            return false
        }
        if (!isCategoryValid()) {
            return false
        }
        if (!isValidStartDate()) {
            return false
        }
        if (!isValidTime()) {
            return false
        }
        if (tag.value != null) {
            if (!isTagsValid()) {
                return false
            }
        }

        if (!isValidDay()) {
            return false
        }

        if(nights.value != null){
            if(!isNightsValid()){
                return false
            }
        }

        if(!isTransportationCostValid()){
            return false
        }

        if(!isActivityCostValid()){
            return false
        }

        return isValid
    }

    fun isValidTime(): Boolean {
        val isValid = true
        val startTime = startTime.value
        if (startTime.isNullOrBlank()) {
            return false
        }
        return isValid
    }

    fun isValidStartDate(): Boolean {
        val isValid = true
        startDate.value ?: return false

        return isValid
    }

    private fun isValidEndDate(): Boolean {
        val isValid = true
        val startDate = startDate.value ?: return false
        val endDate = finishedDate.value ?: return false
        if (endDate.timeInMillis < startDate.timeInMillis) {
            return false
        }
        return isValid
    }

     fun isTagsValid(): Boolean {
        val isValid = true
        val tags = tag.value ?: return false
        val pattern = Pattern.compile("[a-zA-Z,]+")
        if (!pattern.matcher(tags).matches()) {
            return false
        }
        return isValid
    }

    fun isValidDay(): Boolean {
        val isValid = true
        val day = day.value
        if (day.isNullOrBlank()) {
            return false
        }
        try {
            day.toInt()
        } catch (e: NumberFormatException) {
            return false
        }
        return isValid
    }
}
