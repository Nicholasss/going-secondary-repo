package ph.edu.usjr.going

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Location
import java.util.*

@Parcelize
data class OnAddEntryEvent(
    val id: String? = null,
    val travelerCount: String? = null,
    val origin: Location? = null,
    val destination: Location? = null,
    val totalCost: String? = null,
    val title: String? = "",
    val startDate: Calendar? = null,
    val endDate: Calendar? = null,
    val entries: ArrayList<Entry>? = null
) : Parcelable
