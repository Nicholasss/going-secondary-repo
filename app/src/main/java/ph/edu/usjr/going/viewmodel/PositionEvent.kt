package ph.edu.usjr.going.viewmodel

import ph.edu.usjr.going.model.Entry

data class PositionEvent(
    val journalList: ArrayList<Entry> = arrayListOf(),
    val position: Int = -1
)
