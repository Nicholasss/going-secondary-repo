package ph.edu.usjr.going.interfaces

interface OnSetEntryCompleteListener {
    fun onsetEntryComplete()
}