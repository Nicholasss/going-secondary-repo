package ph.edu.usjr.going.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import retrofit2.http.GET

@Parcelize
data class Itinerary(
    @SerializedName("creatorId")
    val creatorId: String = "",
    @SerializedName("travelerCount")
    val travelerCount : Int = 0,
    @SerializedName("origin")
    val origin : Location = Location(),
    @SerializedName("destination")
    val destination : Location = Location(),
    @SerializedName("totalCost")
    val totalCost : Double = 0.0,
    @SerializedName("title")
    val title: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("startDate")
    val startDate : Timestamp = Timestamp(0,0),
    @SerializedName("endDate")
    val endDate : Timestamp = Timestamp(0,0),
    @SerializedName("published")
    val published: Boolean = false,
    @SerializedName("days")
    val days : Long = -1
) :Parcelable