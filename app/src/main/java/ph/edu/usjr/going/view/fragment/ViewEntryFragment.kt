package ph.edu.usjr.going.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.fragment_view_entry.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.*
import ph.edu.usjr.going.databinding.FragmentViewEntryBinding
import ph.edu.usjr.going.interfaces.OnSetEntryCompleteListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.repositories.EntryRepository
import ph.edu.usjr.going.view.activity.AddItineraryActivity
import ph.edu.usjr.going.viewmodel.EntryViewModel
import java.text.SimpleDateFormat
import java.util.*

class ViewEntryFragment : BaseFragment() {
    private lateinit var mEntryVm: EntryViewModel
    private lateinit var mBinding: FragmentViewEntryBinding
    private lateinit var mListener: OnSetEntryCompleteListener
    private var mItineraryId: String? = null
    private lateinit var mEntries: ArrayList<Entry>
    private var mPosition = -1
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mEntryVm = ViewModelProviders.of(this).get(EntryViewModel::class.java)
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_view_entry,
            container,
            false
        )
        mBinding.vm = mEntryVm
        mBinding.lifecycleOwner = this
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        entrySaveBtn.setOnClickListener {
            if (mEntryVm.isValidEntry() && app.auth.currentUser != null && mItineraryId != null) {
                val entryId: String? = if (mEntryVm.id.value != null) {
                    mEntryVm.id.value
                } else {
                    EntryRepository().makeEntryId()
                }
                val user = app.auth.currentUser!!.uid
                val origin = mEntryVm.originLocation.value!!
                val destination = mEntryVm.destinationLocation.value!!
                val day = mEntryVm.day.value!!.toInt() - 1
                val startTime = mEntryVm.startDate.value!!
                val transportation = mEntryVm.transportationId.value!!
                val mode = mEntryVm.category.value!!
                val tags = mEntryVm.tag.value
                val notes = mEntryVm.notes.value
                val modeName = mEntryVm.modeName.value!!
                val transportationCost = mEntryVm.transportationCost.value!!.toDouble()
                val activityCost = mEntryVm.activityCost.value!!.toDouble()

                mEntryVm.title.value = resources.getString(
                    R.string.checkedIn,
                    modeName,
                    destination.address
                )
                val title = mEntryVm.title.value ?: ""
                val nights = mEntryVm.nights.value
                val entry = Entry(
                    entryId,
                    transportationCost,
                    activityCost,
                    Timestamp(startTime.time),
                    null,
                    user,
                    origin,
                    destination,
                    title,
                    mode,
                    tags,
                    notes,
                    day,
                    transportation,
                    nights?.toInt()
                )
                if(mPosition == -1) {
                    mEntries.add(entry)
                }else{
                    mEntries[mPosition] = entry
                }
                EntryRepository.instance.deleteEntries(
                    mItineraryId!!,
                    mEntries,
                    mListener,
                    "Itineraries"
                )
            }
        }

        onEntrySetListener(object : OnSetEntryCompleteListener {
            override fun onsetEntryComplete() {
                if (activity != null) {
                    goToPage(AddItineraryActivity::class.java)
                    activity!!.finish()
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().removeAllStickyEvents()
    }

    /**
     * Listener events
     */
    private fun onEntrySetListener(eventListener: OnSetEntryCompleteListener) {
        mListener = eventListener
    }

    /**
     * Subscriber Events
     */
    @Subscribe(sticky = true)
    fun locationAndTimeEvent(event: LocationTimeEvent) {
        mEntryVm.originLocation.value = event.origin
        mEntryVm.origin.value = event.origin.address
        mEntryVm.destinationLocation.value = event.destination
        mEntryVm.destination.value = event.destination.address
        mEntryVm.day.value = (event.days + 1).toString()
        val calendar = Calendar.getInstance()
        calendar.time = event.startTime.toDate()
        val timeFormat = SimpleDateFormat("hh:mm a", Locale.US)
        mEntryVm.startTime.value = timeFormat.format(calendar.time)
        mEntryVm.startDate.value = calendar
        mEntryVm.category.value = event.category
        mEntryVm.modeName.value = event.modeName
        if (event.category.categoryType == 1) {
            nightsTitleTxt.visibility = View.VISIBLE
            nightsTxt.visibility = View.VISIBLE
        }
        mEntryVm.activityCost.value = event.activityCost.toString()
        if(event.nights != null) {
            mEntryVm.nights.value = event.nights.toString()
        }
    }

    @Subscribe(sticky = true)
    fun transportationEvent(event: TransportationEvent) {
        mEntryVm.transportationName.value = event.transportationName
        mEntryVm.transportationId.value = event.transportationId
        mEntryVm.transportationCost.value = event.transportationCost.toString()
        mEntryVm.tag.value = event.tags
        mEntryVm.notes.value = event.notes
    }

    @Subscribe(sticky = true)
    fun costAndEtcEvent(event: CostAndNotesEvent) {

    }

    @Subscribe(sticky = true)
    fun onAddEntryEvent(event: OnAddEntryEvent) {
        mItineraryId = event.id
        mEntries = if (event.entries != null) {
            event.entries
        } else {
            arrayListOf()
        }
    }
    @Subscribe(sticky = true)
    fun onEditEntryReceived(event : OnEntryReceivedEvent){
        mEntryVm.id.value = event.entry.entryId
        mPosition = event.position
    }
}