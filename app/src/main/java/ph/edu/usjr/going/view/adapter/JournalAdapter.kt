package ph.edu.usjr.going.view.adapter

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.design.card.MaterialCardView
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.journal_row.view.*
import ph.edu.usjr.going.GoingApplication
import ph.edu.usjr.going.R
import ph.edu.usjr.going.interfaces.OnJournalEntryClicked
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.repositories.EntryRepository
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import kotlin.collections.ArrayList

class JournalAdapter(
    private val context: Context,
    private val journalEntryList: ArrayList<Entry>,
    private val listener: OnJournalEntryClicked
) : RecyclerView.Adapter<JournalViewHolder>() {
    @TargetApi(Build.VERSION_CODES.P)
    override fun onBindViewHolder(viewHolder: JournalViewHolder, position: Int) {
        val timeFormat = SimpleDateFormat("hh:mm a", Locale.US)
        if (journalEntryList[position].isCheckedIn == true) {
            viewHolder.title.setTextColor(Color.parseColor("#3302a9"))
            viewHolder.icon.setBackgroundResource(R.drawable.ic_timeline)
            viewHolder.entryCard.setCardBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
            viewHolder.time.visibility = View.VISIBLE
            viewHolder.time.text = timeFormat.format(journalEntryList[position].startTime.toDate())
        } else {
            viewHolder.title.setTextColor(Color.parseColor("#acb1c4"))
            viewHolder.icon.setBackgroundResource(R.drawable.ic_timeline_unchecked_in)
            viewHolder.entryCard.setCardBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.colorTransparent
                )
            )
            viewHolder.entryCard.outlineSpotShadowColor =
                ContextCompat.getColor(context, R.color.colorTransparent)
            viewHolder.entryCard.outlineAmbientShadowColor =
                ContextCompat.getColor(context, R.color.colorTransparent)
            viewHolder.time.visibility = View.INVISIBLE
        }
        viewHolder.title.text = journalEntryList[position].title
        val totalCost = journalEntryList[position].activityCost + journalEntryList[position].transportationCost
        viewHolder.cost.text =
            context.resources.getString(R.string.costEntry, "$", totalCost)
        viewHolder.destination.text =
            context.resources.getString(
                R.string.destinationEntry,
                journalEntryList[position].destination.address
            )
        viewHolder.entryCard.setOnClickListener {
            if (viewHolder.entryCard.isPressed) {
                listener.onDoneSelected(position)
            } else {
                //todo show a message that a checked shit cannot be undone.
            }
        }
    }

    override fun getItemCount(): Int {
        return journalEntryList.size
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): JournalViewHolder {
        return JournalViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.journal_row,
                viewGroup,
                false
            )
        )
    }

}

class JournalViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
    val title: TextView = itemView.journalEntryTitle
    val time: TextView = itemView.journalEntryTimeTxt
    val cost: TextView = itemView.journalEntryCostTxt
    val icon: Button = itemView.journalEntryCategoryBtn
    val destination: TextView = itemView.journalEntryDestinationTxt
    val entryCard: MaterialCardView = itemView.entryJournalCardView
}