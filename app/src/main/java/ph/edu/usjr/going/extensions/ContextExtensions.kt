package ph.edu.usjr.going.extensions

import android.R.style.Theme_Holo_Light_Dialog
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.format.DateUtils
import ph.edu.usjr.going.R
import ph.edu.usjr.going.interfaces.OnBirthDateChangedListener
import ph.edu.usjr.going.interfaces.OnDateSelectedListener
import ph.edu.usjr.going.interfaces.OnTimeSelectedListener
import java.text.SimpleDateFormat
import java.util.*

//this is used to show the date picker dialog
fun Context.showDatePickerDialog(
    calendar: Calendar,
    birthDayListener: OnBirthDateChangedListener? = null,
    dateSelectedListener: OnDateSelectedListener? = null
) {
    val yearCal = calendar.get(Calendar.YEAR)
    val monthCal = calendar.get(Calendar.MONTH)
    val dayCal = calendar.get(Calendar.DAY_OF_MONTH)
    //dialog is created here
    val dialog = DatePickerDialog(
        this as Activity,
        Theme_Holo_Light_Dialog,
        DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            //calendar of the registerVM is set here
            calendar.set(year, month, dayOfMonth)
            birthDayListener?.onBirthDateChanged()
            dateSelectedListener?.onDateSelected()
        },
        yearCal,
        monthCal,
        dayCal
    )
    if (dialog.window != null) {
        if (birthDayListener != null) {
            dialog.datePicker.maxDate = System.currentTimeMillis() - (18 * DateUtils.YEAR_IN_MILLIS)
        } else {
            dialog.datePicker.minDate = System.currentTimeMillis() - 1000
        }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }
}

fun Context.showTimePickerDialog(calendar: Calendar, listener: OnTimeSelectedListener) {
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH)
    val day = calendar.get(Calendar.DAY_OF_MONTH)
    val hourCalendar = calendar.get(Calendar.HOUR_OF_DAY)
    val minuteCalendar = calendar.get(Calendar.MINUTE)
    val dialog = TimePickerDialog(
        this as Activity,
        TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            calendar.set(year, month, day, hour, minute)
            listener.onTimeSelected()
        }, hourCalendar, minuteCalendar, false
    )
    if (dialog.window != null) {
        dialog.setTitle(R.string.selectTime)
        dialog.show()
    }
}

fun Context.setCalendarName(calendar: Calendar): String {
    return getString(
        R.string.date,
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH) + 1,
        calendar.get(Calendar.DAY_OF_MONTH)
    )
}
