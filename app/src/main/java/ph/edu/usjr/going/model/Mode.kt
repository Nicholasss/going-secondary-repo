package ph.edu.usjr.going.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Mode(
    val id : String = "",
    val category : Int = -1,
    val name : String = "",
    val frequency : Int = 0
) : Parcelable