package ph.edu.usjr.going.repositories

import com.google.firebase.Timestamp
import ph.edu.usjr.going.GoingApplication
import ph.edu.usjr.going.interfaces.OnJournalCreatedListener
import ph.edu.usjr.going.interfaces.OnJournalReceivedListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Itinerary
import ph.edu.usjr.going.model.Journal

class JournalRepository{
    companion object {
        private lateinit var mInstance: JournalRepository
        val instance: JournalRepository
            get() {
                if (!::mInstance.isInitialized) {
                    mInstance = JournalRepository()
                }
                return mInstance
            }
    }
    //todo israel finish activity view itinerary.
    //todo show only 1 journal.
    //todo di pwede mo start ug another journal
    //todo shaun please make dialog in case ganahan mo start ug another journal ang user.
    fun getJournal(listener: OnJournalReceivedListener){
        var isAllPublished = true
        val user = GoingApplication.instance.auth.currentUser
        GoingApplication.instance.database.collection("Journals").get()
            .addOnSuccessListener {querySnapshot->
                for(index in querySnapshot){
                    val journal = index.toObject(Journal::class.java)
                    if(journal.endDate==null && journal.creatorId == user?.uid){
                        listener.onJournalReceived(journal)
                        isAllPublished = false
                        break
                    }
                }
                if(isAllPublished){
                    listener.onJournalReceived(null)
                }
            }
    }
    fun makeJournal(itinerary : Itinerary,entriesList: ArrayList<Entry>,listener: OnJournalCreatedListener){
        val itineraryUpdate = Itinerary(itinerary.creatorId,itinerary.travelerCount,itinerary.origin,itinerary.destination,itinerary.totalCost,itinerary.title,itinerary.id,itinerary.startDate,itinerary.endDate,true)
        GoingApplication.instance.database.collection("Itineraries").document(itinerary.id).set(itineraryUpdate)
        val ref = GoingApplication.instance.database.collection("Journals").document()
        val id = ref.id
        val journal = Journal(itinerary.creatorId,itinerary.travelerCount,itinerary.origin,itinerary.destination,itinerary.totalCost,itinerary.title,id,
            Timestamp.now(),null)
        GoingApplication.instance.database.collection("Journals").document(id).set(journal)
            .addOnSuccessListener {
                EntryRepository().setEntries(entriesList,id,null,"Journals",listener,journal)
            }
    }


    fun endJournal(journalId: String, path: String) {
        val ref = GoingApplication.instance.database.collection(path).document(journalId)
        ref.update("endDate", Timestamp.now())
    }
}