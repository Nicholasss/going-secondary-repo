package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Mode

interface OnModeReceivedListener {
    fun onModesReceived(modeList : ArrayList<Mode>)
    fun onModeReceived(mode : Mode)
}