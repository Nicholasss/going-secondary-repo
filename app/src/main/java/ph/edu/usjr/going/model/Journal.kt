package ph.edu.usjr.going.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Journal(
    val creatorId: String = "",
    val travelerCount : Int = 0,
    val origin : Location = Location(),
    val destination : Location = Location(),
    val totalCost : Double = 0.0,
    val title: String = "",
    val id: String = "",
    val startDate : Timestamp = Timestamp(0,0),
    val endDate : Timestamp? = null
) : Parcelable