package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Itinerary

interface OnItineraryReceivedListener {
    fun onItinerariesReceived(itineraryList : ArrayList<Itinerary>)
    fun onItineraryReceived(itinerary : Itinerary)
}