package ph.edu.usjr.going.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Bindable
import android.net.Uri
import android.util.Log
import ph.edu.usjr.going.extensions.isValidEmail
import java.util.*
import java.util.regex.Pattern


class RegisterViewModel : ViewModel() {
    var image: Uri? = null
    val birthDateCalendar: Calendar = Calendar.getInstance()

    @Bindable
    val email = MutableLiveData<String>()
    @Bindable
    val password = MutableLiveData<String>()
    @Bindable
    val firstName = MutableLiveData<String>()
    @Bindable
    val lastName = MutableLiveData<String>()
    @Bindable
    val birthday = MutableLiveData<String>()

    private fun isValidPassword(): Boolean {
        val isValid = true
        val password = password.value
        if (password.isNullOrBlank()) {
            return false
        }
        if (password.length < 6) {
            return false
        }
        return isValid
    }

    private fun isValidEmail(): Boolean {
        val isValid = true
        val email = email.value
        if (email.isNullOrBlank()) {
            return false
        }
        if (!email.isValidEmail()) {
            return false
        }
        return isValid
    }

    fun isValidName(): Boolean {
        val isValid = true
        val firstName = firstName.value
        val lastName = lastName.value
        val pattern = Pattern.compile("^([a-zA-Z]+?)([-\\s'][a-zA-Z]+)*?\$")
        val firstNameMatcher = pattern.matcher(firstName)
        val lastNameMatcher = pattern.matcher(lastName)
        if (firstName.isNullOrBlank()) {
            return false
        }
        if (lastName.isNullOrBlank()) {
            return false
        }
        if (!firstNameMatcher.matches()) {
            return false

        }
        if (!lastNameMatcher.matches()) {
            return false
        }
        return isValid
    }

    fun isValidDate(): Boolean {
        val isValid: Boolean
        val mBirthday = birthday.value
        if (mBirthday.isNullOrBlank() || birthDateCalendar.get(Calendar.YEAR) <= 1880) {
            return false
        }

        birthDateCalendar.isLenient = false
        isValid = try {
            birthDateCalendar.time
            true
        } catch (e: Exception) {
            Log.d("errorHere", "${e.message} ${e.cause}")
            false
        }
        return isValid
    }

    fun checkInput(): Boolean {
        val isValid = true
        if (!isValidEmail()) {
            return false
        }
        if (!isValidName()) {
            return false
        }
        if (!isValidPassword()) {
            return false
        }
        if (!isValidDate()) {
            return false
        }
        return isValid
    }
}