package ph.edu.usjr.going.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Bindable
import com.google.firebase.Timestamp
import ph.edu.usjr.going.model.Entry

class JournalViewModel : ViewModel(){
    @Bindable
    val title = MutableLiveData<String>()
    @Bindable
    val travelerCount = MutableLiveData<String>()
    @Bindable
    val originText = MutableLiveData<String>()
    @Bindable
    val destinationText = MutableLiveData<String>()
    @Bindable
    val cost = MutableLiveData<String>()
    init{
        cost.value = "0.0"
    }
    val entriesCollection = MutableLiveData<ArrayList<Entry>>()
    init{
        entriesCollection.value = arrayListOf()
    }
    val startDate = MutableLiveData<Timestamp>()
    val id = MutableLiveData<String>()

    fun isEntriesListValid(): Boolean {
        val isValid = true
        val list = entriesCollection.value ?: return false
        if (list.isEmpty()) {
            return false
        }
        return isValid
    }

}