package ph.edu.usjr.going.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.itinerary_row.view.*
import ph.edu.usjr.going.R
import ph.edu.usjr.going.interfaces.OnItinerarySelectedListener
import ph.edu.usjr.going.model.Itinerary

class ItineraryAdapter(
    private val itineraryList: ArrayList<Itinerary>,
    private val listener: OnItinerarySelectedListener
) : RecyclerView.Adapter<ItineraryViewHolder>() {
    override fun onBindViewHolder(viewHolder: ItineraryViewHolder, position: Int) {
        viewHolder.entryTitle.text = itineraryList[position].title
        viewHolder.editImage.setOnClickListener {
            listener.onItinerarySelected(position)
        }
    }

    override fun getItemCount(): Int {
        return itineraryList.size
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ItineraryViewHolder {
        return ItineraryViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.itinerary_row,
                viewGroup,
                false
            )
        )
    }
}

class ItineraryViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
    val entryTitle: TextView = itemView.journalTitleTxt
    val editImage: ImageView = itemView.journalRandImg
}