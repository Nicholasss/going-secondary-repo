//package ph.edu.usjr.going.view.fragment
//
//import android.arch.lifecycle.ViewModelProviders
//import android.databinding.DataBindingUtil
//import android.os.Bundle
//import android.support.v4.app.FragmentManager
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Toast
//import kotlinx.android.synthetic.main.fragment_entry_cost_and_notes.*
//import org.greenrobot.eventbus.EventBus
//import org.greenrobot.eventbus.Subscribe
//import ph.edu.usjr.going.*
//import ph.edu.usjr.going.databinding.FragmentEntryCostAndNotesBinding
//import ph.edu.usjr.going.viewmodel.EntryViewModel
//
//class EntryCostAndNotes : BaseFragment() {
//    private lateinit var mFragmentManager : FragmentManager
//    private lateinit var mEntryVm: EntryViewModel
//    private lateinit var mBinding: FragmentEntryCostAndNotesBinding
//    private var mModeType = -1
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        mEntryVm = ViewModelProviders.of(this).get(EntryViewModel::class.java)
//        mBinding = DataBindingUtil.inflate(
//            inflater,
//            R.layout.fragment_entry_cost_and_notes,
//            container,
//            false
//        )
//        mBinding.vm = mEntryVm
//        mBinding.lifecycleOwner = this
//        return mBinding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        entryCostsNotesNextBtn.setOnClickListener {
//            if (mEntryVm.isTransportationCostValid() && mEntryVm.isActivityCostValid() && activity!= null) {
//
//                if (mModeType == 1) {
//                    EventBus.getDefault().postSticky(
//                        CostAndNotesEvent(
//                            transportationCost,
//                            notes,
//                            tags
//                        )
//                    )
//                } else {
//                    EventBus.getDefault().postSticky(
//                        CostAndNotesEvent(
//                            transportationCost,
//                            null,
//                            tags
//                        )
//                    )
//                }
//                mFragmentManager = activity!!.supportFragmentManager
//                val fragment = ViewEntryFragment()
//                val transaction = mFragmentManager.beginTransaction()
//                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right)
//                transaction.replace(R.id.flItineary, fragment).addToBackStack(null).commit()
//            } else{
//                Toast.makeText(context,"Some inputs are invalid, Please double check and try again!",
//                    Toast.LENGTH_LONG).show()
//            }
//        }
//    }
//
//    override fun onStart() {
//        super.onStart()
//        EventBus.getDefault().register(this)
//    }
//
//    override fun onStop() {
//        EventBus.getDefault().unregister(this)
//        super.onStop()
//    }
//
//    @Subscribe(sticky = true)
//        fun onModeEventReceived(event: LocationTimeEvent) {
//            if (event.category.categoryType == 1) {
//                etCostPerNight.visibility = View.VISIBLE
//                mModeType = event.category.categoryType
//            }
//    }
//    @Subscribe(sticky = true)
//    fun onEditReceivedEvent(event : OnEntryReceivedEvent){
//
//
//    }
//}