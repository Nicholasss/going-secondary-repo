package ph.edu.usjr.going.view.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.libraries.places.widget.Autocomplete
import kotlinx.android.synthetic.main.activity_guided.*
import ph.edu.usjr.going.*
import ph.edu.usjr.going.interfaces.JournalPlaceHolderApi
import ph.edu.usjr.going.model.Itinerary
import ph.edu.usjr.going.model.Location
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GuidedActivity : BaseActivity() {

    private lateinit var mApiService: JournalPlaceHolderApi
    private lateinit var originLocation: Location
    private lateinit var destinationLocation: Location
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guided)
        mApiService = ApiUtils().getAPIService()
        guidedOrigin.setOnClickListener{
            val intent = SetUpAutoComplete().buildAutoComplete(this)
            startActivityForResult(intent, Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE)
        }
        guidedDestination.setOnClickListener{
            val intent = SetUpAutoComplete().buildAutoComplete(this)
            startActivityForResult(intent, Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE)
        }

        guidedBackBtn.setOnClickListener {
            finish()
        }

        button3.setOnClickListener {
            if (!guidedOrigin.text.isNullOrBlank() &&
                !guidedDestination.text.isNullOrBlank() &&
                !guidedCost.text.isNullOrBlank() &&
                !guidedTravelerCount.text.isNullOrBlank() &&
                !guidedTravelSpan.text.isNullOrBlank()
            ) {
                val user = GoingApplication.instance.auth.currentUser!!.uid
                sendPost(user,originLocation,destinationLocation,guidedCost.text.toString().toLong(),guidedTravelerCount.text.toString().toInt(),guidedTravelSpan.text.toString().toInt())
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        } else if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        }

    }


    private fun handleOnActivityResult(resultCode: Int, data: Intent?, requestCode: Int) {
        when {
            resultCode == Activity.RESULT_OK && data != null -> {
                val place = Autocomplete.getPlaceFromIntent(data)
                if (place.latLng != null && place.address != null) {
                    if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
                        originLocation = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.address!!
                        )
                        guidedOrigin.setText(place.address)
                    } else if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE) {
                        destinationLocation = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.address!!
                        )
                        guidedDestination.setText(place.address)
                    }

                }
            }
        }
    }
    private fun sendPost(userId : String,origin : Location,destination : Location, cost : Long, count : Int,span : Int){
        val map = HashMap<String, Any> ()
        map.put("span",span)
        map.put("preferredCost",cost)
        map.put("preferredOrigin",origin )
        map.put("preferredDestination",destination)
        map.put("travelerCount",count)
        mApiService.savePost(userId,map).enqueue(object : Callback<Itinerary>{
            override fun onFailure(call: Call<Itinerary>, t: Throwable) {
                Toast.makeText(this@GuidedActivity,"$t",Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Itinerary>, response: Response<Itinerary>) {
                Toast.makeText(this@GuidedActivity,response.body().toString(),Toast.LENGTH_LONG).show()
                val res: Itinerary = response.body() as Itinerary
                Log.d("test", response.body().toString())
            }
        })
    }
}
