package ph.edu.usjr.going.view.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_entry_check_in.*
import kotlinx.android.synthetic.main.fragment_view_entry.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.*
import ph.edu.usjr.going.databinding.ActivityEntryCheckInBinding
import ph.edu.usjr.going.interfaces.OnEntriesReceivedListener
import ph.edu.usjr.going.interfaces.OnEntryReceivedListener
import ph.edu.usjr.going.interfaces.OnJournalReceivedListener
import ph.edu.usjr.going.interfaces.OnModeReceivedListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Journal
import ph.edu.usjr.going.model.Mode
import ph.edu.usjr.going.repositories.EntryRepository
import ph.edu.usjr.going.repositories.JournalRepository
import ph.edu.usjr.going.repositories.ModeRepository
import ph.edu.usjr.going.viewmodel.EntryViewModel
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class EntryCheckInActivity : BaseActivity() {
    private lateinit var mEntryVm: EntryViewModel
    private lateinit var mJournalEntryReceivedListener: OnEntryReceivedListener
    private lateinit var mEntryReceivedListener: OnEntriesReceivedListener
    private lateinit var mJournalEntryChecker: OnEntryReceivedListener
    private lateinit var mJournalReceivedListener: OnJournalReceivedListener
    private lateinit var mModeReceivedListener: OnModeReceivedListener

    private lateinit var mJournal: Journal
    private var mEntryId: String? = null
    private var mJournalId: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry_check_in)
        mEntryVm = ViewModelProviders.of(this).get(EntryViewModel::class.java)
        DataBindingUtil.setContentView<ActivityEntryCheckInBinding>(
            this,
            R.layout.activity_entry_check_in
        ).apply {
            this.vm = mEntryVm
            this.lifecycleOwner = this@EntryCheckInActivity
        }

        setupListeners()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        JournalRepository.instance.getJournal(mJournalReceivedListener)
        EntryRepository.instance.getEntry(
            mJournalId!!,
            mEntryId!!,
            "Journals",
            mJournalEntryChecker
        )
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    private fun getTime(calendar: Calendar) {
        val timeFormat = SimpleDateFormat("hh:mm a", Locale.US)
        mEntryVm.startTime.value = timeFormat.format(calendar.time)
    }

    /**
     * Listeners
     */
    private fun setupListeners() {
        checkInBtn.setOnClickListener {
            if (mJournalId != null && mEntryId != null)
                EntryRepository.instance.getEntry(
                    mJournalId!!,
                    mEntryId!!,
                    "Journals",
                    mJournalEntryReceivedListener
                )
        }

        checkInBackBtn.setOnClickListener {
            finish()
        }

        onEntriesReceivedListener(object : OnEntriesReceivedListener {
            override fun onEntriesReceived(entries: ArrayList<Entry>) {
                EventBus.getDefault().postSticky(OnJournalAndEntriesEvent(mJournal, entries))
                goToPage(StartJournalActivity::class.java)
            }
        })

        onJournalReceivedListener(object : OnJournalReceivedListener {
            override fun onJournalReceived(journal: Journal?) {
                if (journal != null)
                    mJournal = journal
            }
        })

        entryChecker(object : OnEntryReceivedListener {
            override fun onEntryReceived(entry: Entry) {
                if (entry.isCheckedIn == true) {
                    checkInBtn.text = getText(R.string.checked_in)
                    checkInBtn.setBackgroundResource(R.drawable.button_bg_deactivated)
                    journalEntryTimeTitleCheckIn.text = "Time"
                } else {
                    checkInBtn.text = getText(R.string.check_in)
                    checkInBtn.setBackgroundResource(R.drawable.button_bg_primary)
                    journalEntryTimeTitleCheckIn.text = "Planned Time"
                }
            }
        })

        setJournalEntryRetrievedListener(object : OnEntryReceivedListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onEntryReceived(entry: Entry) {
                if (entry.isCheckedIn == false) {
                    val builder = AlertDialog.Builder(this@EntryCheckInActivity)
                    val title = entry.title
                    builder.setTitle(title)
                    builder.setMessage("Are you sure you want to check this entry in? This action cannot be undone.")
                    builder.setPositiveButton("YES") { dialog, which ->
                        EntryRepository.instance.checkInEntry(mJournalId!!, mEntryId!!, "Journals")
                        EntryRepository.instance.getEntries(
                            mJournalId!!,
                            mEntryReceivedListener,
                            "Journals"
                        )
                        Toast.makeText(
                            applicationContext,
                            "Entry Checked In",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    builder.setNegativeButton("No") { dialog, which ->
                        Toast.makeText(
                            applicationContext,
                            "Entry Not Checked In",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                } else {
                    Toast.makeText(
                        this@EntryCheckInActivity,
                        "Entry already checked in. Cannot be undone.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })

        onModeReceivedListener(object : OnModeReceivedListener {
            override fun onModeReceived(mode: Mode) {
                mEntryVm.transportationName.value = mode.name
            }

            override fun onModesReceived(modeList: ArrayList<Mode>) {
            }
        })
    }

    private fun onModeReceivedListener(eventListener: OnModeReceivedListener) {
        mModeReceivedListener = eventListener
    }

    private fun setJournalEntryRetrievedListener(eventListener: OnEntryReceivedListener) {
        mJournalEntryReceivedListener = eventListener
    }

    private fun entryChecker(eventListener: OnEntryReceivedListener) {
        mJournalEntryChecker = eventListener
    }

    private fun onEntriesReceivedListener(eventListener: OnEntriesReceivedListener) {
        mEntryReceivedListener = eventListener
    }

    private fun onJournalReceivedListener(eventListener: OnJournalReceivedListener) {
        mJournalReceivedListener = eventListener
    }

    @Subscribe(sticky = true)
    fun journalAndEntryEventReceiver(event: JournalAndEntryEvent) {
        mEntryId = event.entryId
        mJournalId = event.journalId
    }

    @Subscribe(sticky = true)
    fun entryReceiver(event: OnEntryReceivedEvent) {
        mEntryVm.title.value = event.entry.title
        val transportation =
            ModeRepository.instance.getMode(event.entry.transportation, mModeReceivedListener)
        mEntryVm.transportationName.value = transportation.toString()
        val calendar = Calendar.getInstance()
        calendar.time = event.entry.startTime.toDate()
        getTime(calendar)
        mEntryVm.day.value = event.entry.day.toString()
        mEntryVm.nights.value = event.entry.nights.toString()
        mEntryVm.notes.value = event.entry.notes
        mEntryVm.transportationCost.value = event.entry.transportationCost.toString()
        mEntryVm.activityCost.value = event.entry.activityCost.toString()
        val totalCost = mEntryVm.transportationCost.value + mEntryVm.activityCost.value
        mEntryVm.cost.value = totalCost
    }
}

