package ph.edu.usjr.going.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserInfo(val uid: String = "", val email: String = "", val photoUrl: String = "") :
    Parcelable