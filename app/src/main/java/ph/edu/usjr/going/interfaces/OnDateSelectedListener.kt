package ph.edu.usjr.going.interfaces

interface OnDateSelectedListener {
    fun onDateSelected()
}