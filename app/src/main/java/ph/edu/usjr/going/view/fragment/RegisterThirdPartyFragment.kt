package ph.edu.usjr.going.view.fragment


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_register_third_party.*
import ph.edu.usjr.going.R
import ph.edu.usjr.going.databinding.FragmentRegisterThirdPartyBinding
import ph.edu.usjr.going.extensions.showDatePickerDialog
import ph.edu.usjr.going.interfaces.OnBirthDateChangedListener
import ph.edu.usjr.going.model.Traveler
import ph.edu.usjr.going.view.activity.LandingActivity
import ph.edu.usjr.going.view.activity.LogInActivity
import ph.edu.usjr.going.viewmodel.RegisterViewModel
import java.util.*


class RegisterThirdPartyFragment : BaseFragment() {
    private lateinit var mListener: OnBirthDateChangedListener
    private lateinit var mRegisterVm: RegisterViewModel
    private lateinit var mBinding: FragmentRegisterThirdPartyBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRegisterVm = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_register_third_party,
            container,
            false
        )
        mBinding.vm = mRegisterVm
        mBinding.lifecycleOwner = this
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etBirthDay3rd.setOnClickListener {
            selectBirthday()
        }
        createAccountBtn3rd.setOnClickListener {
            createAccount()
        }
        backbtn.setOnClickListener {
            goToPage(LogInActivity::class.java)
        }
    }

    private fun selectBirthday() {
        setCustomEventListener(object : OnBirthDateChangedListener {
            override fun onBirthDateChanged() {
                mRegisterVm.birthday.value =
                    getString(
                        R.string.date,
                        mRegisterVm.birthDateCalendar.get(Calendar.YEAR),
                        mRegisterVm.birthDateCalendar.get(Calendar.MONTH) + 1,
                        mRegisterVm.birthDateCalendar.get(Calendar.DAY_OF_MONTH)
                    )
            }
        })
        context?.showDatePickerDialog(mRegisterVm.birthDateCalendar, mListener)
    }

    private fun createAccount() {
        val userInfo = app.getFireBaseUserInfo()
        if (mRegisterVm.isValidDate() && mRegisterVm.isValidName()) {
            val traveler = Traveler(
                userInfo.uid,
                userInfo.email.trim(),
                mRegisterVm.firstName.value!!.capitalize().trim(),
                mRegisterVm.lastName.value!!.capitalize().trim(),
                userInfo.photoUrl,
                mRegisterVm.birthDateCalendar.timeInMillis
            )
            app.database.collection("Travelers").document(userInfo.uid)
                .set(traveler)
                .addOnSuccessListener {
                    goToPage(LandingActivity::class.java)
                }
                .addOnFailureListener {
                    //todo shaun error message when failed to save in database
                }
        }
    }

    private fun setCustomEventListener(eventListener: OnBirthDateChangedListener) {
        mListener = eventListener
    }
}