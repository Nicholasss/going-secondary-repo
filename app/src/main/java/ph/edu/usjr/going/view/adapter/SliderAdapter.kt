package ph.edu.usjr.going.view.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import ph.edu.usjr.going.R

class SliderAdapter(private var context: Context) : PagerAdapter() {

    lateinit var layoutInflater: LayoutInflater
    private var slideImages: IntArray = intArrayOf(
        R.drawable.pioneer,
        R.drawable.guided
    )

    private var slideHeadings = arrayOf(
        "Unguided",
        "Guided"
    )

    private var slideDescription: Array<String> = arrayOf(
        "Travel the world on your own terms with a personalized\n" +
                "travel plan. Where you go, how many days you spend\n" +
                "there and what kind of travel and accommodations you " +
                "choose are entirely up to you.",
        "Go on an adventure with a dynamically\n" +
                "generated itinerary tailored your way. Forget spending\n" +
                "time creating one and go on an adventure at a tap of a button."
    )

    override fun isViewFromObject(p0: View, p1: Any): Boolean = p0 == p1 as ConstraintLayout

    override fun getCount(): Int = slideImages.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.slide_layout, container, false)
        val slideImageView: ImageView = view.findViewById(R.id.slide_image)
        val slideHeading: TextView = view.findViewById(R.id.slide_heading)
        val slideDesc: TextView = view.findViewById(R.id.slide_desc)

        slideImageView.setImageResource(slideImages[position])
        slideHeading.text = slideHeadings[position]
        slideDesc.text = slideDescription[position]

        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }

}