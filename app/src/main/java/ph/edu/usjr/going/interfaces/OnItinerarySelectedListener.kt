package ph.edu.usjr.going.interfaces

interface OnItinerarySelectedListener {
    fun onItinerarySelected(position : Int)
}