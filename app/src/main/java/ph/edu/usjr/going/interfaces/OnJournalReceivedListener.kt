package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Journal

interface OnJournalReceivedListener{
    fun onJournalReceived(journal: Journal? = null)
}