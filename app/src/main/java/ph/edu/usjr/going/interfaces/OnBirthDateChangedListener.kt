package ph.edu.usjr.going.interfaces


interface OnBirthDateChangedListener {
    //this is a listener used to check if the birthday has been set
    fun onBirthDateChanged()
}