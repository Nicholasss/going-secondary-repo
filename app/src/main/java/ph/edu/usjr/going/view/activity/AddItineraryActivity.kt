package ph.edu.usjr.going.view.activity

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import kotlinx.android.synthetic.main.activity_add_itinerary.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.*
import ph.edu.usjr.going.databinding.ActivityAddItineraryBinding
import ph.edu.usjr.going.extensions.setCalendarName
import ph.edu.usjr.going.extensions.showDatePickerDialog
import ph.edu.usjr.going.interfaces.OnDateSelectedListener
import ph.edu.usjr.going.interfaces.OnEntryButtonClickedListener
import ph.edu.usjr.going.interfaces.OnItineraryReceivedListener
import ph.edu.usjr.going.interfaces.OnSetEntryCompleteListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Itinerary
import ph.edu.usjr.going.model.Location
import ph.edu.usjr.going.repositories.EntryRepository
import ph.edu.usjr.going.repositories.ItineraryRepository
import ph.edu.usjr.going.view.adapter.EntryAdapter
import ph.edu.usjr.going.viewmodel.ItineraryViewModel
import java.util.*
import kotlin.collections.ArrayList

@SuppressWarnings("unused")
class AddItineraryActivity : BaseActivity() {
    private lateinit var mAdapter: EntryAdapter
    private lateinit var mButtonClickedListener: OnEntryButtonClickedListener
    private lateinit var mOnEntrySetListener: OnSetEntryCompleteListener
    private lateinit var mItineraryReceivedListener: OnItineraryReceivedListener
    private lateinit var mItineraryVm: ItineraryViewModel
    private lateinit var mDateListener: OnDateSelectedListener
    //Life cycle methods of activity.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_itinerary)
        mItineraryVm = ViewModelProviders.of(this).get(ItineraryViewModel::class.java)
        DataBindingUtil
            .setContentView<ActivityAddItineraryBinding>(
                this,
                R.layout.activity_add_itinerary
            )
            .apply {
                this.lifecycleOwner = this@AddItineraryActivity
                this.vm = mItineraryVm
            }
        setOnClickListeners()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        initRecyclerView()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().removeAllStickyEvents()
    }

    private fun setOnClickListeners() {

        itineraryAddBackButton.setOnClickListener {
            finish()
        }
        addEntryTxt.setOnClickListener {
            if (mItineraryVm.isItineraryValid()) {
                postEventBus()
                goToPage(AddEntryActivity::class.java)
            } else {
                //todo shaun make a better UI for this.
                Toast.makeText(
                    this@AddItineraryActivity,
                    "Some entries are invalid, kindly double check the entries.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        createItineraryBtn.setOnClickListener {
            if (mItineraryVm.isItineraryValid() && app.auth.currentUser != null) {
                mItineraryVm.setDays()
                val id = mItineraryVm.id.value
                val user = app.auth.currentUser!!
                val title = mItineraryVm.travelName.value!!
                val count = mItineraryVm.travelerCount.value!!.toInt()
                val origin = mItineraryVm.originLoc.value!!
                val destination = mItineraryVm.destinationLoc.value!!
                val totalCost = mItineraryVm.totalCost.value!!.toDouble()
                val startDate = mItineraryVm.startDateCalendar.value!!
                val endDate = mItineraryVm.endDateCalendar.value!!
                val days = mItineraryVm.days.value!!
                ItineraryRepository().setItinerary(
                    user,
                    title, count,
                    origin, destination,
                    totalCost,
                    startDate,
                    endDate,
                    false,
                    id,
                    days,
                    mItineraryReceivedListener
                )

                addEntryTxt.visibility = View.VISIBLE
                addEntryTxt.alpha = 0.0f
                addEntryTxt.animate()
                    .alpha(1.0f)
                    .duration = 1000
                etTravelName.isFocusable = false
                etTravelCount.isFocusable = false
                etDestination.isClickable = false
                etOrigin.isClickable = false
                etStartDate.isClickable = false
                etEndDate.isClickable = false
                createItineraryBtn.visibility = View.INVISIBLE
                createItineraryBar?.title = resources.getString(R.string.add_entry)
            }
            else{
                Toast.makeText(this,"Some inputs are invalid, Please double check and try again!",Toast.LENGTH_LONG).show()
            }
        }

        etStartDate.setOnClickListener {
            if (mItineraryVm.startDateCalendar.value == null) {
                mItineraryVm.startDateCalendar.value = Calendar.getInstance()
            }
            selectDate(mItineraryVm.startDateCalendar.value!!, true)
        }

        etEndDate.setOnClickListener {
            if (mItineraryVm.endDateCalendar.value == null) {
                mItineraryVm.endDateCalendar.value = Calendar.getInstance()
            }
            selectDate(mItineraryVm.endDateCalendar.value!!, false)
        }

        etDestination.setOnClickListener {
            startActivityForResult(
                SetUpAutoComplete().buildAutoComplete(this),
                Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE
            )
        }

        etOrigin.setOnClickListener {
            startActivityForResult(
                SetUpAutoComplete().buildAutoComplete(this),
                Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE
            )
        }

        setButtonsClickedListener(object : OnEntryButtonClickedListener {
            override fun onDeleteSelected(position: Int, id: String?) {
                mItineraryVm.subtractTotalCostFromEntryPosition(position)
                if (id != null && mItineraryVm.id.value != null) {
                    EntryRepository.instance.deleteEntry(mItineraryVm.id.value!!,id,"Itineraries")
                }
                mItineraryVm.entriesCollection.value?.removeAt(position)
                mAdapter.notifyDataSetChanged()
            }

            override fun onEditSelected(position: Int) {
                if (mItineraryVm.isEntriesListValid()) {
                    postEventBus()
                    if (mItineraryVm.isEntriesListValid()) {
                        EventBus.getDefault().postSticky(
                            OnEntryReceivedEvent(
                                mItineraryVm.entriesCollection.value!![position],
                                position
                            )
                        )
                        goToPage(AddEntryActivity::class.java)
                    }
                }
            }

        })

        onEntrySetListener(object : OnSetEntryCompleteListener {
            override fun onsetEntryComplete() {
                goToPage(ViewItineraryActivity::class.java)
                finish()
            }
        })

        onItineraryReceivedListener(object : OnItineraryReceivedListener {
            override fun onItinerariesReceived(itineraryList: ArrayList<Itinerary>) {
            }

            override fun onItineraryReceived(itinerary: Itinerary) {
                setUpItineraryVm(itinerary)
            }
        })
    }

    private fun initRecyclerView() {
        if (mItineraryVm.isEntriesListValid()) {
            mAdapter =
                EntryAdapter(
                    mItineraryVm.entriesCollection.value!!,
                    this@AddItineraryActivity,
                    mButtonClickedListener
                )
            rvItineraryEntries.layoutManager =
                LinearLayoutManager(this, LinearLayout.VERTICAL, false)
            rvItineraryEntries.adapter = mAdapter
        }
    }

    private fun selectDate(calendar: Calendar, isStartDate: Boolean) {
        setDateSelectedListener(object : OnDateSelectedListener {
            override fun onDateSelected() {
                if (isStartDate) {
                    mItineraryVm.startDate.value =
                        this@AddItineraryActivity.setCalendarName(calendar)
                } else {
                    mItineraryVm.endDate.value = this@AddItineraryActivity.setCalendarName(calendar)
                }
            }
        })
        this@AddItineraryActivity.showDatePickerDialog(calendar, null, mDateListener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        } else if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        }

    }

    private fun handleOnActivityResult(resultCode: Int, data: Intent?, requestCode: Int) {
        when {
            resultCode == Activity.RESULT_OK && data != null -> {
                val place = Autocomplete.getPlaceFromIntent(data)
                if (place.latLng != null && place.address != null) {
                    if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
                        mItineraryVm.originLoc.value = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.address!!
                        )
                        mItineraryVm.origin.value = place.address
                    } else if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE) {
                        mItineraryVm.destinationLoc.value = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.address!!
                        )
                        mItineraryVm.destination.value = place.address
                    }
                }
            }
            resultCode == AutocompleteActivity.RESULT_ERROR && data != null -> {
                val snackbar = Snackbar.make(
                    addItineraryRootLayout, "An unforseen error has occured. Please try again.",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
                val status = Autocomplete.getStatusFromIntent(data)
                Log.d("errorMessage", status.statusMessage)
            }
        }
    }

    private fun setDate() {
        if (mItineraryVm.startDateCalendar.value != null) {
            mItineraryVm.startDate.value = setCalendarName(mItineraryVm.startDateCalendar.value!!)
        }
        if (mItineraryVm.endDateCalendar.value != null) {
            mItineraryVm.endDate.value = setCalendarName(mItineraryVm.endDateCalendar.value!!)
        }
    }

    private fun postEventBus() {
        val id = mItineraryVm.id.value
        val title = mItineraryVm.travelName.value
        val count = mItineraryVm.travelerCount.value
        val origin = mItineraryVm.originLoc.value
        val destination = mItineraryVm.destinationLoc.value
        val totalCost = mItineraryVm.totalCost.value
        val startDate = mItineraryVm.startDateCalendar.value
        val endDate = mItineraryVm.endDateCalendar.value
        val event =
            OnAddEntryEvent(
                id,
                count,
                origin,
                destination,
                totalCost,
                title,
                startDate,
                endDate,
                mItineraryVm.entriesCollection.value
            )
        EventBus.getDefault().postSticky(event)
    }

    //listener methods
    private fun setDateSelectedListener(eventListener: OnDateSelectedListener) {
        mDateListener = eventListener
    }

    private fun setButtonsClickedListener(eventListener: OnEntryButtonClickedListener) {
        mButtonClickedListener = eventListener
    }

    private fun onEntrySetListener(eventListener: OnSetEntryCompleteListener) {
        mOnEntrySetListener = eventListener
    }

    private fun onItineraryReceivedListener(eventListener: OnItineraryReceivedListener) {
        mItineraryReceivedListener = eventListener
    }

    @Subscribe(sticky = true)
    fun onAddEntryCollectionEvent(event: ArrayList<Entry>) {
        mItineraryVm.entriesCollection.value = event
        setDate()
        arrangeByDate()
    }

    @Subscribe(sticky = true)
    fun onAddEntryEvent(event: OnAddEntryEvent) {
        mItineraryVm.totalCost.value = mItineraryVm.addToTotalCost().toString()

        mItineraryVm.inputValues(
            event.id,
            event.title,
            event.travelerCount?.toInt(),
            event.origin,
            event.destination,
            event.startDate,
            event.endDate
        )
    }

    @Subscribe(sticky = true)
    fun onEditItineraryEvent(event: Itinerary) {
        createItineraryBar?.title = resources.getString(R.string.edit_itinerary)
        setUpItineraryVm(event)
    }

    private fun arrangeByDate() {
        if (mItineraryVm.isEntriesListValid()) {
                val mutableList = mItineraryVm.entriesCollection.value as MutableList<Entry>
                mutableList.sortWith(compareBy({ it.day }, { it.startTime }))
                mItineraryVm.entriesCollection.value = mutableList as ArrayList<Entry>
            mItineraryVm.originLoc.value = mutableList.first().origin
            mItineraryVm.origin.value = mutableList.first().origin.address
            mItineraryVm.destinationLoc.value = mutableList.last().destination
            mItineraryVm.destination.value = mutableList.last().destination.address
        }
        mAdapter.notifyDataSetChanged()
    }

    private fun setUpItineraryVm(event: Itinerary) {
        mItineraryVm.totalCost.value = mItineraryVm.addToTotalCost().toString()
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        startDate.time = event.startDate.toDate()
        endDate.time = event.endDate.toDate()
        mItineraryVm.inputValues(
            event.id,
            event.title,
            event.travelerCount,
            event.origin,
            event.destination,
            startDate,
            endDate
        )
        setDate()
    }


}


//createItineraryBtn.setOnClickListener {
//    if (mItineraryVm.isItineraryValid() && app.auth.currentUser != null) {
//        val id = mItineraryVm.id.value
//        val user = app.auth.currentUser!!
//        val title = mItineraryVm.travelName.value!!
//        val count = mItineraryVm.travelerCount.value!!.toInt()
//        val origin = mItineraryVm.originLoc.value!!
//        val destination = mItineraryVm.destinationLoc.value!!
//        val totalCost = mItineraryVm.totalCost.value!!.toDouble()
//        val startDate = mItineraryVm.startDateCalendar.value!!
//        val endDate = mItineraryVm.endDateCalendar.value!!
//        val entryList = mItineraryVm.entriesCollection.value!!
//        val days = mItineraryVm.days.value!!
//        ItineraryRepository().setItinerary(
//            user,
//            title,
//            count,
//            origin,
//            destination,
//            totalCost,
//            startDate,
//            endDate,
//            entryList,
//            false,
//            id,
//            mOnEntrySetListener,
//            days
//        )
//        if (id == null) {
//            goToPage(LandingActivity::class.java)
//            finish()
//        }
//    }
//}