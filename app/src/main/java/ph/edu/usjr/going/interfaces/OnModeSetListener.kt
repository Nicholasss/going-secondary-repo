package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Mode

interface OnModeSetListener {
    fun onModeSet(mode : Mode)
}