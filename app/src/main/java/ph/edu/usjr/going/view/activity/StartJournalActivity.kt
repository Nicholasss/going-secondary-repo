package ph.edu.usjr.going.view.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import android.widget.PopupMenu
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.activity_start_journal.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.*
import ph.edu.usjr.going.databinding.ActivityStartJournalBinding
import ph.edu.usjr.going.interfaces.OnJournalEntryClicked
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.repositories.EntryRepository
import ph.edu.usjr.going.repositories.ItineraryRepository
import ph.edu.usjr.going.repositories.JournalRepository
import ph.edu.usjr.going.view.adapter.EntryAdapter
import ph.edu.usjr.going.view.adapter.JournalAdapter
import ph.edu.usjr.going.viewmodel.JournalViewModel

class StartJournalActivity : BaseActivity() {
    private lateinit var mJournalVm: JournalViewModel
    private lateinit var mJournalAdapter: JournalAdapter
    private lateinit var mJournalClickedListener: OnJournalEntryClicked
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_journal)
        mJournalVm = ViewModelProviders.of(this).get(JournalViewModel::class.java)
        DataBindingUtil.setContentView<ActivityStartJournalBinding>(
            this,
            R.layout.activity_start_journal
        ).apply {
            this.vm = mJournalVm
            this.lifecycleOwner = this@StartJournalActivity
        }
        onSetUpListeners()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        initModeRecyclerView()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun onSetUpListeners() {

        journalMenuButton.setOnClickListener {
            val popupMenu = PopupMenu(this, it)
            popupMenu.setOnMenuItemClickListener { view ->
                when (view.itemId) {
                    R.id.journalEndBtn -> {
                        //journal repo call here
                        JournalRepository.instance.endJournal(mJournalVm.id.value!!, "Journals")
                        finish()
                        true
                    }
                    else -> false
                }
            }
            popupMenu.inflate(R.menu.journal_options)
            popupMenu.show()
        }

        journalBackButton.setOnClickListener {
            goToPage(LandingActivity::class.java)
            finish()
        }
        setJournalEntryListener(object : OnJournalEntryClicked {
            override fun onDoneSelected(position: Int) {
                if (mJournalVm.entriesCollection.value != null && mJournalVm.id.value != null) {
                    //val entry = mJournalVm.entriesCollection.value!![position]
                    //val journalEntry = Entry(entry.entryId,entry.cost,entry.startTime, Timestamp.now(),entry.creatorId,entry.origin,entry.destination,entry.title,entry.type,entry.tag,entry.notes)
                    // EntryRepository.instance.updateEntry(mJournalVm.id.value!!,journalEntry,"Journals")
                    val journalId = mJournalVm.id.value!!
                    val entryPosition = mJournalVm.entriesCollection.value!![position]
                    val entryId = entryPosition.entryId!!
                    EventBus.getDefault().postSticky(JournalAndEntryEvent(journalId, entryId))
                    EventBus.getDefault().postSticky(
                        OnEntryReceivedEvent(
                            mJournalVm.entriesCollection.value!![position],
                            position
                        )
                    )
                    goToPage(EntryCheckInActivity::class.java)
                    Log.d("isChecked", entryPosition.isCheckedIn.toString())
                }
            }
        })
    }

    private fun arrangeByDate() {
        if (mJournalVm.isEntriesListValid()) {
            val mutableList = mJournalVm.entriesCollection.value as MutableList<Entry>
            mutableList.sortWith(compareBy({ it.day }, { it.startTime }))
            mJournalVm.entriesCollection.value = mutableList as ArrayList<Entry>
            mJournalVm.originText.value = mutableList.first().origin.address
            mJournalVm.originText.value = mutableList.last().destination.address
        }
        mJournalAdapter.notifyDataSetChanged()
    }

    private fun initModeRecyclerView() {
        if (mJournalVm.entriesCollection.value != null) {
            mJournalAdapter = JournalAdapter(
                this@StartJournalActivity,
                mJournalVm.entriesCollection.value!!,
                mJournalClickedListener
            )
            rvJournalEntry.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
            rvJournalEntry.adapter = mJournalAdapter
        }
    }

    private fun setJournalEntryListener(eventListener: OnJournalEntryClicked) {
        mJournalClickedListener = eventListener
    }

    @Subscribe(sticky = true)
    fun onJournalReceivedEvent(event: OnJournalAndEntriesEvent) {
        mJournalVm.id.value = event.journal.id
        mJournalVm.title.value = event.journal.title
        mJournalVm.cost.value = event.journal.totalCost.toString()
        mJournalVm.travelerCount.value = event.journal.travelerCount.toString()
        mJournalVm.destinationText.value = event.journal.destination.address
        mJournalVm.originText.value = event.journal.origin.address
        mJournalVm.entriesCollection.value = event.entries
        mJournalVm.startDate.value = event.journal.startDate
        arrangeByDate()
    }
}
