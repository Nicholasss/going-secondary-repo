package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Journal

interface OnJournalCreatedListener {
    fun onJournalCreated(journal : Journal)
}