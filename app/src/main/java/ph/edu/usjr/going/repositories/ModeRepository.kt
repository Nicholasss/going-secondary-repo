package ph.edu.usjr.going.repositories

import ph.edu.usjr.going.GoingApplication
import ph.edu.usjr.going.interfaces.OnModeReceivedListener
import ph.edu.usjr.going.interfaces.OnModeSetListener
import ph.edu.usjr.going.model.Mode


class ModeRepository {
    companion object {
        private lateinit var mInstance: ModeRepository
        val instance: ModeRepository
            get() {
                if (!::mInstance.isInitialized) {
                    mInstance = ModeRepository()
                }
                return mInstance
            }
    }
    fun getMode(id : String,listener : OnModeReceivedListener){
        GoingApplication.instance.database.collection("Mode").document(id).get()
            .addOnSuccessListener {
                val mode = it.toObject(Mode::class.java)
                if(mode != null) {
                    listener.onModeReceived(mode)
                }
            }
    }
    fun getModes(listener: OnModeReceivedListener, category: Int) {
        val modeList = arrayListOf<Mode>()
        GoingApplication.instance.database.collection("Mode").get()
            .addOnSuccessListener { querySnapshot ->
                for (index in querySnapshot) {
                    val mode = index.toObject(Mode::class.java)
                    if (category == mode.category) {
                        modeList.add(mode)
                    }
                }
                modeList.sortByDescending {
                    it.frequency
                }
                listener.onModesReceived(modeList)
            }
    }

    fun setMode(listener: OnModeSetListener, mode : Mode){
        var modeExists = false
        GoingApplication.instance.database.collection("Mode").get()
            .addOnSuccessListener {querySnapshot->
                for(index in querySnapshot){
                    val dbMode = index.toObject(Mode::class.java)
                    if(dbMode.name.toLowerCase() == mode.name.toLowerCase()){
                        modeExists = true
                        listener.onModeSet(dbMode)
                        break
                    }
                }
                if(!modeExists){
                    val ref = GoingApplication.instance.database.collection("Mode").document()
                    val id = ref.id
                    val modeForDb = Mode(id,mode.category,mode.name,mode.frequency)
                    ref.set(modeForDb).addOnSuccessListener {
                        listener.onModeSet(modeForDb)
                    }
                }
            }
    }
}