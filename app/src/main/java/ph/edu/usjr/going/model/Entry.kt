package ph.edu.usjr.going.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Entry(
    val entryId: String? = null,
    val transportationCost: Double = 0.0,
    val activityCost: Double = 0.0,
    val startTime : Timestamp = Timestamp(0,0),
    val finishedDate: Timestamp? = null,
    val creatorId: String = "",
    val origin: Location = Location(),
    val destination: Location = Location(),
    val title: String = "",
    val type: Category = Category(),
    val tag: String? = null,
    val notes: String? = null,
    val day : Int = -1,
    val transportation : String = "",
    val nights: Int? = null,
    var isCheckedIn : Boolean? = false
) : Parcelable