package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Entry

interface OnEntriesReceivedListener {
    fun onEntriesReceived(entries : ArrayList<Entry>)
}