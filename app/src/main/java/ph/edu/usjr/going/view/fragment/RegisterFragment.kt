package ph.edu.usjr.going.view.fragment

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_register.*
import ph.edu.usjr.going.Constants
import ph.edu.usjr.going.viewmodel.RegisterViewModel
import android.databinding.DataBindingUtil
import android.net.Uri
import ph.edu.usjr.going.databinding.FragmentRegisterBinding
import ph.edu.usjr.going.model.Traveler
import ph.edu.usjr.going.view.activity.LandingActivity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import ph.edu.usjr.going.R
import ph.edu.usjr.going.interfaces.OnBirthDateChangedListener
import ph.edu.usjr.going.extensions.showDatePickerDialog
import ph.edu.usjr.going.view.activity.LogInActivity
import java.util.*


class RegisterFragment : BaseFragment() {
    private lateinit var mListener: OnBirthDateChangedListener
    private lateinit var mRegisterVm: RegisterViewModel
    private lateinit var mBinding: FragmentRegisterBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRegisterVm = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        mBinding.vm = mRegisterVm
        mBinding.lifecycleOwner = this
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etBirthDay.setOnClickListener {
            selectBirthday()
        }
        selectImgBtn.setOnClickListener {
            checkPermissions()
        }
        createAccountBtn.setOnClickListener {
            createAccount()
        }
        backbtn.setOnClickListener {
            goToPage(LogInActivity::class.java)
        }
    }


    private fun hasPermissions(permission: String): Boolean {
        if (context != null) {
            if (ActivityCompat.checkSelfPermission(
                    this.context!!,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                return true
            }
            return false
        }
        return false
    }

    private fun checkPermissions() {
        val permission = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        if (context != null) {
            if (!hasPermissions(permission[0])) {
                requestPermissions(
                    permission,
                    Constants.PERMISSION_GALLERY
                )
            } else {
                selectPhoto()
            }
        }
    }

    private fun selectPhoto() {
        val requestImage = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(requestImage, Constants.PERMISSION_GALLERY)
    }

    private fun selectBirthday() {
        setCustomEventListener(object : OnBirthDateChangedListener {
            override fun onBirthDateChanged() {
                mRegisterVm.birthday.value =
                    getString(
                        R.string.date,
                        mRegisterVm.birthDateCalendar.get(Calendar.YEAR),
                        mRegisterVm.birthDateCalendar.get(Calendar.MONTH) + 1,
                        mRegisterVm.birthDateCalendar.get(Calendar.DAY_OF_MONTH)
                    )
            }
        })
        context?.showDatePickerDialog(mRegisterVm.birthDateCalendar, mListener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.GALLERY_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                mRegisterVm.image = data.data
                selectImgBtn.setImageURI(mRegisterVm.image)
            }
            permissionErrTxt.visibility = View.GONE
        } else {
            permissionErrTxt.visibility = View.VISIBLE
        }
    }

    private fun createAccount() {
        if (mRegisterVm.checkInput()) {
            app.auth
                .createUserWithEmailAndPassword(
                    "${mRegisterVm.email.value?.trim()}",
                    "${mRegisterVm.password.value?.trim()}"
                )
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val key = it.result!!.user.uid
                        if (mRegisterVm.image != null) {
                            app.storage
                                .putFile(mRegisterVm.image as Uri)
                                .addOnSuccessListener {
                                    app.storage.downloadUrl
                                        .addOnSuccessListener { profileUri ->
                                            val traveler = Traveler(
                                                key,
                                                "${mRegisterVm.email.value}",
                                                mRegisterVm.firstName.value!!.capitalize(),
                                                mRegisterVm.lastName.value!!.capitalize(),
                                                "$profileUri",
                                                mRegisterVm.birthDateCalendar.timeInMillis
                                            )
                                            app.database.collection("Travelers").document(key)
                                                .set(traveler)
                                                .addOnSuccessListener {
                                                    goToPage(LandingActivity::class.java)
                                                    databaseErrTxt.visibility = View.GONE
                                                }
                                                .addOnFailureListener {
                                                    databaseErrTxt.visibility = View.VISIBLE
                                                }
                                        }
                                        .addOnFailureListener {
                                            //Internal error for download url not retrieved
                                        }
                                    storageErrTxt.visibility = View.GONE
                                }
                                .addOnFailureListener {
                                    storageErrTxt.visibility = View.VISIBLE
                                }
                        } else {
                            val traveler = Traveler(
                                key,
                                "${mRegisterVm.email.value}",
                                mRegisterVm.firstName.value!!.capitalize(),
                                mRegisterVm.lastName.value!!.capitalize(),
                                "null",
                                mRegisterVm.birthDateCalendar.timeInMillis
                            )
                            app.database.collection("Travelers").document(key).set(traveler)
                            goToPage(LogInActivity::class.java)
                        }
                    }
                }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.PERMISSION_GALLERY ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectPhoto()
                    permissionErrTxt.visibility = View.GONE
                } else {
                    permissionErrTxt.visibility = View.VISIBLE
                }
        }
    }

    private fun setCustomEventListener(eventListener: OnBirthDateChangedListener) {
        mListener = eventListener
    }
}