package ph.edu.usjr.going.interfaces

interface OnTimeSelectedListener{
    fun onTimeSelected()
}