package ph.edu.usjr.going.repositories

import android.os.Build
import android.support.annotation.RequiresApi
import com.google.firebase.Timestamp
import ph.edu.usjr.going.GoingApplication
import ph.edu.usjr.going.OnEntryReceivedEvent
import ph.edu.usjr.going.interfaces.OnEntriesReceivedListener
import ph.edu.usjr.going.interfaces.OnEntryReceivedListener
import ph.edu.usjr.going.interfaces.OnJournalCreatedListener
import ph.edu.usjr.going.interfaces.OnSetEntryCompleteListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Journal
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class EntryRepository {
    companion object {
        private lateinit var mInstance: EntryRepository
        val instance: EntryRepository
            get() {
                if (!::mInstance.isInitialized) {
                    mInstance = EntryRepository()
                }
                return mInstance
            }
    }

    fun setEntries(
        entry: ArrayList<Entry>,
        id: String? = null,
        listener: OnSetEntryCompleteListener? = null,
        path: String,
        journalListener: OnJournalCreatedListener? = null,
        journal: Journal? = null
    ) {
        if (id != null) {
            entry.forEach {
                if (it.entryId != null) {
                    GoingApplication.instance.database.collection(path)
                        .document(id)
                        .collection("entries").document(it.entryId).set(it)
                }
            }
            listener?.onsetEntryComplete()
            if (journal != null) {
                journalListener?.onJournalCreated(journal)
            }
        }
    }

    fun makeEntryId(): String {
        val ref = GoingApplication.instance.database.collection("Itineraries").document()
        return ref.id
    }

    fun getEntries(documentId: String, listener: OnEntriesReceivedListener, path: String) {
        val entries = arrayListOf<Entry>()
        GoingApplication.instance.database.collection(path).document(documentId)
            .collection("entries").get()
            .addOnSuccessListener {
                for (index in it) {
                    val entry = index.toObject(Entry::class.java)
                    entries.add(entry)
                }
                listener.onEntriesReceived(entries)
            }
    }

    fun deleteEntries(
        id: String,
        entries: ArrayList<Entry>,
        listener: OnSetEntryCompleteListener,
        path: String
    ) {
        GoingApplication.instance.database.collection(path).document(id)
            .collection("entries").get()
            .addOnSuccessListener { snapshot ->
                snapshot.forEach { queryDocumentSnapshot ->
                    entries.forEach {
                        if (it.entryId != queryDocumentSnapshot.id) {
                            queryDocumentSnapshot.reference.delete()
                        }
                    }
                }
                setEntries(entries, id, listener, path)
            }
    }
    fun deleteEntry(documentId : String, entryId : String, path: String){
        GoingApplication.instance.database.collection(path).document(documentId)
            .collection("entries").document(entryId).delete()
    }
    fun updateEntry(id: String, entry: Entry,path : String){
        if(entry.entryId != null) {
            val ref = GoingApplication.instance.database.collection(path).document(id)
                .collection("entries").document(entry.entryId)
            ref.update("finishedDate", Timestamp.now())
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun checkInEntry(id: String, entryId: String, path: String) {
        val ref = GoingApplication.instance.database.collection(path).document(id)
            .collection("entries").document(entryId)
        ref.update("checkedIn", true)
        ref.update("startTime", Timestamp.now())
    }

    fun getEntry(id: String, entryId: String, path: String, listener: OnEntryReceivedListener) {
        GoingApplication.instance.database.collection(path).document(id)
            .collection("entries").document(entryId).get()
            .addOnSuccessListener {
                val entry = it.toObject(Entry::class.java)
                if (entry != null) {
                    listener.onEntryReceived(entry)
                }
            }
    }
}