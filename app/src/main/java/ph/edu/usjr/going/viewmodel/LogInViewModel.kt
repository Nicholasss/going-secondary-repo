package ph.edu.usjr.going.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Bindable
import com.facebook.login.LoginManager
import ph.edu.usjr.going.extensions.isValidEmail

class LogInViewModel : ViewModel() {
    //bind variable for email connected with XML.
    @Bindable
    val email = MutableLiveData<String>()
    //bind variable for password connected with XML.
    @Bindable
    val password = MutableLiveData<String>()
    //Facebook Log in manager.
    val logInManager: LoginManager
        get() {
            return LoginManager.getInstance()
        }

    /*
    @desc checks validity of log in.
    @return bool - success or failure.
     */
    fun isValidLogIn(): Boolean {
        val email = email.value
        if (email == null || email.isEmpty() || !email.isValidEmail()) {
            return false
        }

        val password = password.value
        if (password == null || password.isEmpty()) {
            return false
        }

        return true
    }
}
