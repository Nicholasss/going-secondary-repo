package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Itinerary
import ph.edu.usjr.going.model.Journal
import ph.edu.usjr.going.model.Location
import retrofit2.Call
import retrofit2.http.*

interface JournalPlaceHolderApi {
    //todo chan get (itinerary/journal)
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
   )

    @POST("/itineraries/recommended/{id}/")
    fun savePost(
        @Path("id") user: String,
        @Body map: HashMap<String,Any>
    ): Call<Itinerary>
}