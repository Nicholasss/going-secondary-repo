package ph.edu.usjr.going.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupMenu
import android.widget.TextView
import kotlinx.android.synthetic.main.entry_row.view.*
import ph.edu.usjr.going.R
import ph.edu.usjr.going.interfaces.OnEntryButtonClickedListener
import ph.edu.usjr.going.model.Entry
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EntryAdapter(
    private val entriesList: ArrayList<Entry>,
    private val context: Context,
    private val listener: OnEntryButtonClickedListener? = null
) : RecyclerView.Adapter<EntryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryViewHolder {
        return EntryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.entry_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return entriesList.size
    }

    override fun onBindViewHolder(viewHolder: EntryViewHolder, position: Int) {
        val timeFormat = SimpleDateFormat("hh:mm a", Locale.US)
        viewHolder.title.text = entriesList[position].title
        val totalCost = entriesList[position].activityCost + entriesList[position].transportationCost
        viewHolder.cost.text =
            context.resources.getString(R.string.costEntry, "$", totalCost)
        viewHolder.destination.text =
            context.resources.getString(
                R.string.destinationEntry,
                entriesList[position].destination.address
            )
        viewHolder.time.text = timeFormat.format(entriesList[position].startTime.toDate())
        /*viewHolder.deleteBtn.setOnClickListener {
            entriesList.removeAt(position)
            notifyDataSetChanged()
        }
        viewHolder.editBtn.setOnClickListener {
            listener.onEditSelected(position)
        }*/
        if (listener != null) {
            viewHolder.entryOptions.visibility = View.VISIBLE
            viewHolder.entryOptions.setOnClickListener {
                val popupMenu = PopupMenu(context, it)
                popupMenu.setOnMenuItemClickListener { view ->
                    when (view.itemId) {
                        R.id.editEntryBtn -> {
                            listener.onEditSelected(position)
                            true
                        }
                        R.id.deleteEntryBtn -> {
                            listener.onDeleteSelected(position,entriesList[position].entryId)

                            true
                        }
                        else -> false
                    }
                }
                popupMenu.inflate(R.menu.entry_options)
                popupMenu.show()
            }
        } else {
            viewHolder.entryOptions.visibility = View.INVISIBLE
        }
    }
}

class EntryViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
    val time: TextView = ItemView.entryTimeTxt
    //val categoryImg = ItemView.entryCategoryImg
    val title: TextView = ItemView.entryTitleTxt
    val destination: TextView = ItemView.entryDestinationTxt
    val cost: TextView = ItemView.entryCostTxt
    val entryOptions: Button = ItemView.optionsBtn
}