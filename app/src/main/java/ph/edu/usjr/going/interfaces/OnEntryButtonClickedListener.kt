package ph.edu.usjr.going.interfaces

interface OnEntryButtonClickedListener{
    fun onDeleteSelected(position : Int,id : String? = null)
    fun onEditSelected(position : Int)
}