package ph.edu.usjr.going

data class TransportationEvent (
    val transportationId : String,
    val transportationName : String,
    val transportationCost : Double = 0.0,
    val notes : String? = null,
    val tags : String? = null
)