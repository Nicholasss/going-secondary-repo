package ph.edu.usjr.going.view.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import ph.edu.usjr.going.GoingApplication

abstract class BaseFragment : Fragment() {
    val app: GoingApplication
        get() {
            return GoingApplication.instance
        }

    /**
    @desc used to start activity
    @params Class<out Activity> clz - the activity to be started.
    @params Bundle? extras - used if there is a need to pass extras.
     */
    fun goToPage(clz: Class<out Activity>, extras: Bundle? = null) {
        val intent = Intent(context, clz)
        //if there are extras, it will be placed here
        if (extras != null) {
            intent.putExtras(extras)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        //perform the intent
        startActivity(intent)
    }
}
