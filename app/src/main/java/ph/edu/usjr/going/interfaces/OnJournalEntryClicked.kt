package ph.edu.usjr.going.interfaces

interface OnJournalEntryClicked {
    fun onDoneSelected(position : Int)
}