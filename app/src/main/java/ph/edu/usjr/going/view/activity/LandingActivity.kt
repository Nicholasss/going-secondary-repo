package ph.edu.usjr.going.view.activity

import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_landing.*
import kotlinx.android.synthetic.main.landing_data_container.*
import org.greenrobot.eventbus.EventBus
import ph.edu.usjr.going.OnJournalAndEntriesEvent
import ph.edu.usjr.going.R
import ph.edu.usjr.going.interfaces.OnEntriesReceivedListener
import ph.edu.usjr.going.interfaces.OnItineraryReceivedListener
import ph.edu.usjr.going.interfaces.OnItinerarySelectedListener
import ph.edu.usjr.going.interfaces.OnJournalReceivedListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Itinerary
import ph.edu.usjr.going.model.Journal
import ph.edu.usjr.going.repositories.EntryRepository
import ph.edu.usjr.going.repositories.ItineraryRepository
import ph.edu.usjr.going.repositories.JournalRepository
import ph.edu.usjr.going.view.adapter.ItineraryAdapter

class LandingActivity : BaseActivity() {
    private lateinit var mAdapter: ItineraryAdapter
    private lateinit var mItineraryListener: OnItinerarySelectedListener
    private lateinit var mItineraryReceiverListener: OnItineraryReceivedListener
    private lateinit var mJournalReceivedListener : OnJournalReceivedListener
    private lateinit var mEntryReceivedListener : OnEntriesReceivedListener
    private lateinit var mItineraryList: ArrayList<Itinerary>
    private lateinit var mJournal : Journal
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)
        setUpListeners()
        addItineraryBtn.setOnClickListener {
            goToPage(SelectItineraryTypeActivity::class.java)
        }
        journalRandImg.setOnClickListener {
            EntryRepository.instance.getEntries(mJournal.id,mEntryReceivedListener,"Journals")
        }
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.title = "Going"

        // Initialize the action bar drawer toggle instance
        val drawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.nav_drawer_open,
            R.string.nav_drawer_close
        ) {}
        drawerToggle.isDrawerIndicatorEnabled = true
        drawer_layout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        //items handler
        /*nav_view.setNavigationItemSelectedListener{
            when (it.itemId){
                R.id.user_profile
                }

            }
            // Close the drawer
            drawer_layout.closeDrawer(GravityCompat.START)
            true
        }*/
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().removeAllStickyEvents()
        if (app.auth.currentUser != null) {
            progress_itineraries.visibility = View.VISIBLE
            onItineraryReceivedListener(object : OnItineraryReceivedListener {
                override fun onItinerariesReceived(itineraryList: ArrayList<Itinerary>) {
                    mItineraryList = itineraryList
                    //setuplistenerwasher
                    initRecyclerView()
                    progress_itineraries.visibility = View.GONE
                }

                override fun onItineraryReceived(itinerary: Itinerary) {
                }
            })
            JournalRepository.instance.getJournal(mJournalReceivedListener)
            ItineraryRepository().getItineraries(app.auth.currentUser!!, mItineraryReceiverListener)

        }
    }

    private fun setUpListeners() {
        onItinerarySelectedListener(object : OnItinerarySelectedListener {
            override fun onItinerarySelected(position: Int) {
                EventBus.getDefault().postSticky(mItineraryList[position])
                goToPage(ViewItineraryActivity::class.java)

            }
        })

        onJournalReceivedListener(object : OnJournalReceivedListener{
            override fun onJournalReceived(journal: Journal?) {
                if(journal != null){
                    mJournal = journal
                    journalTitleTxt.text = journal.title
                    journalListContainer.visibility = View.VISIBLE
                }
                else{
                    journalListContainer.visibility = View.GONE
                }
            }
        })

        onEntriesReceivedListener(object : OnEntriesReceivedListener{
            override fun onEntriesReceived(entries: ArrayList<Entry>) {
                EventBus.getDefault().postSticky(OnJournalAndEntriesEvent(mJournal,entries))
                goToPage(StartJournalActivity::class.java)
            }
        })
    }

    private fun initRecyclerView() {

        mAdapter = ItineraryAdapter(mItineraryList, mItineraryListener)
        rvItineraryList.layoutManager =
            LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        rvItineraryList.adapter = mAdapter
    }

    private fun onItinerarySelectedListener(eventListener: OnItinerarySelectedListener) {
        mItineraryListener = eventListener
    }

    private fun onItineraryReceivedListener(eventListener: OnItineraryReceivedListener) {
        mItineraryReceiverListener = eventListener
    }

    private fun onJournalReceivedListener(eventListener : OnJournalReceivedListener){
        mJournalReceivedListener = eventListener
    }

    private fun onEntriesReceivedListener(eventListener: OnEntriesReceivedListener){
        mEntryReceivedListener = eventListener
    }
}
