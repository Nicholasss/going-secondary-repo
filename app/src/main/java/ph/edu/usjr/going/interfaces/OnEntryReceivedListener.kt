package ph.edu.usjr.going.interfaces

import ph.edu.usjr.going.model.Entry

interface OnEntryReceivedListener {
    fun onEntryReceived(entry : Entry)
}