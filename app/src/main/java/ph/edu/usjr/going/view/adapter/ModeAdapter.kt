package ph.edu.usjr.going.view.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.mode_row.view.*
import ph.edu.usjr.going.R
import ph.edu.usjr.going.interfaces.OnModeSelectedListener
import ph.edu.usjr.going.model.Mode

class ModeAdapter(
    private var mode: ArrayList<Mode>,
    private val modeListener: OnModeSelectedListener,
    private val isTransportation : Boolean
) : RecyclerView.Adapter<ModeViewHolder>() {
    private var mCount = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModeViewHolder {
        return ModeViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.mode_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return mode.size
    }

    override fun onBindViewHolder(viewHolder: ModeViewHolder, position: Int) {
        viewHolder.text.text = mode[position].name
        viewHolder.row.setOnClickListener {
            mCount = position
            notifyDataSetChanged()
        }
        if (mCount == position) {
            viewHolder.row.modeNameTxt.setBackgroundResource(R.drawable.mode_pills_selected)
            viewHolder.row.modeNameTxt.setTextColor(Color.parseColor("#ffffff"))
            modeListener.onSelectedListener(position,isTransportation)
        } else {
            viewHolder.row.modeNameTxt.setBackgroundResource(R.drawable.mode_pills)
            viewHolder.row.modeNameTxt.setTextColor(Color.parseColor("#3302a9"))
        }
    }

    fun filterList(searchList: ArrayList<Mode>) {
        mode = searchList
        notifyDataSetChanged()
    }
}

class ModeViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
    //val img : ImageView = ItemView.modeImg
    val text: TextView = ItemView.modeNameTxt
    val row: View = ItemView
}