package ph.edu.usjr.going.view.activity

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.location.Geocoder
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.*
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.firebase.firestore.DocumentChange
import kotlinx.android.synthetic.main.fragment_add_entry.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.*
import ph.edu.usjr.going.databinding.ActivityAddEntryBinding
import ph.edu.usjr.going.interfaces.*
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Location
import ph.edu.usjr.going.model.Mode
import ph.edu.usjr.going.repositories.ModeRepository
import ph.edu.usjr.going.view.adapter.ModeAdapter
import ph.edu.usjr.going.view.fragment.EntryLocationAndTimeFragment
import ph.edu.usjr.going.viewmodel.EntryViewModel
import java.util.*
import kotlin.collections.ArrayList

class AddEntryActivity : BaseActivity() {
    private var mClicked = false
    private lateinit var mEntryVm: EntryViewModel
    private lateinit var mModeAdapter: ModeAdapter
    private lateinit var mModeSearchAdapter: ModeAdapter
    private lateinit var mTransportationAdapter: ModeAdapter
    //   private lateinit var mDateListener: OnDateSelectedListener
    private lateinit var mTimeListener: OnTimeSelectedListener
    private lateinit var mModeListener: OnModeReceivedListener
    private lateinit var mModeSelectedListener: OnModeSelectedListener
    private lateinit var mModeSelectedOnSearchListener: OnModeSelectedListener
    private lateinit var mModeSetListener: OnModeSetListener
    private lateinit var mLocationListener: LocationListener
    private lateinit var mLocationManager: LocationManager
    private lateinit var mModeSearchList: ArrayList<Mode>
    private lateinit var mTransportationMode: ArrayList<Mode>
    private lateinit var mModeList: ArrayList<Mode>
    private lateinit var mEntryList: ArrayList<Entry>
    private lateinit var mFragmentManager : FragmentManager
    private var requestMode = 0
    private var mModeName: String = ""
    private var mMode: Mode? = null
    private var mTransportationName: String = ""
    private var mTransportation: Mode? = null
    private var mType = -1
    private var mEntryEditPosition: Int = -1
    private var mEntryId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_entry)
        mEntryVm = ViewModelProviders.of(this).get(EntryViewModel::class.java)
        mModeList = arrayListOf()
        mTransportationMode = arrayListOf()
        DataBindingUtil
            .setContentView<ActivityAddEntryBinding>(
                this,
                R.layout.activity_add_entry
            )
            .apply {
                this.lifecycleOwner = this@AddEntryActivity
                this.vm = mEntryVm
            }
        mFragmentManager = supportFragmentManager
        mLocationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        changeFragment(EntryLocationAndTimeFragment())
//        setListeners()
//        setOnClickListeners()
//        initModeTransportationView()
//        initModeSearchView()
//        etAddEntryMode.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(string: Editable?) {
//                if (!mClicked) {
//                    filter(string.toString())
//                }
//            }
//
//            override fun onTextChanged(string: CharSequence?, start: Int, before: Int, count: Int) {
//            }
//
//            override fun beforeTextChanged(
//                string: CharSequence?,
//                start: Int,
//                count: Int,
//                after: Int
//            ) {
//            }
//
//        })
//        categorySpinner.setSelection(0)
    }

    override fun onStart() {
        super.onStart()
//        val type = intent?.extras?.getInt(Constants.KEY_CATEGORY_TYPE, -1)
        EventBus.getDefault().register(this)
//        if (type != null) {
//            mType = type
//            //setCategory(type)
//        }
//        if (mType != -1) {
//
//            initModeRecyclerView()
//            checkForUpdates()
//        }
//        if (mType != 0) {
//            etEntryDestination.hint = "Check In"
//        }

    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().removeAllStickyEvents()
    }

    private fun setOnClickListeners() {
        //CATEGORY SPINNER
//        categorySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                mType = position+1
//                initModeRecyclerView()
//                checkForUpdates()
//            }
//
//        }
//        etEntryDaySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                mEntryVm.day.value = position
//            }
//
//        }

        //MODE SPINNER HERE

        etEntryDestination.setOnClickListener {
            val intent = SetUpAutoComplete().buildAutoComplete(this@AddEntryActivity)
            startActivityForResult(intent, Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE)
        }

        etEntryOrigin.setOnClickListener {
            val intent = SetUpAutoComplete().buildAutoComplete(this@AddEntryActivity)
            startActivityForResult(intent, Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE)
        }


//        etEntryStartTime.setOnClickListener {
//            selectTime()
//        }
//
//        etAddEntryMode.setOnClickListener {
//            mClicked = false
//            mMode = null
//        }
//
//        addEntryBtn.setOnClickListener {
//            if (mMode != null) {
//                ModeRepository.instance.setMode(mModeSetListener, mMode!!)
//            } else if (!mEntryVm.transportationName.value.isNullOrBlank()) {
//                mEntryVm.transportationName.value = mEntryVm.transportationName.value!!.capitalizeAndTrim()
//                mModeName = mEntryVm.transportationName.value!!
//                ModeRepository.instance.setMode(
//                    mModeSetListener,
//                    Mode("", mType, mEntryVm.transportationName.value!!, 0)
//                )
//            }

        mLocationListener = object : LocationListener {
            override fun onLocationChanged(location: android.location.Location?) {
                if (requestMode == 0 && location != null) {
                    mEntryVm.originLocation.value =
                        getAddress(location.latitude, location.longitude)
                    mEntryVm.origin.value = mEntryVm.originLocation.value!!.address
                }
            }

            override fun onProviderDisabled(provider: String?) {
                startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }

            override fun onProviderEnabled(provider: String?) {
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }
        }
        originCurrentLocBtn.setOnClickListener {
            getCurrentLocation()
        }
    }


    private fun setListeners() {

//        addEntryBackBtn.setOnClickListener {
//            finish()
//        }

//        setModeReceivedListener(object : OnModeReceivedListener {
//            override fun onModesReceived(modeList: ArrayList<Mode>) {
//                mModeList = arrayListOf()
//                mModeList = modeList
//                initModeRecyclerView()
//                checkForUpdates()
//            }
//
//            override fun onModeReceived(transportationName: Mode) {
//                mMode = transportationName
//                mModeName = transportationName.name
//            }
//        })
//
//        setModeSelectedListener(object : OnModeSelectedListener {
//            override fun onSelectedListener(position: Int,isTransportation : Boolean) {
//                if(isTransportation){
//                    Toast.makeText(
//                        this@AddEntryActivity,
//                        mTransportationMode[position].name,
//                        Toast.LENGTH_LONG
//                    ).show()
//                    mTransportation = mTransportationMode[position]
//                    mTransportationName = mTransportationMode[position].name
//                }
//                else {
//                    Toast.makeText(
//                        this@AddEntryActivity,
//                        mModeList[position].name,
//                        Toast.LENGTH_LONG
//                    )
//                        .show()
//                    mMode = mModeList[position]
//                    mModeName = mModeList[position].name
//                    mEntryVm.transportationName.value = null
//                }
//            }
//        })
//
//        setModeSelectedOnSearchListener(object : OnModeSelectedListener {
//            override fun onSelectedListener(position: Int, isTransportation : Boolean) {
//                mClicked = true
//                mEntryVm.transportationName.value = mModeSearchList[position].name
//                mMode = mModeSearchList[position]
//                mModeName = mModeSearchList[position].name
//                mModeSearchList.clear()
//                mModeSearchAdapter.notifyDataSetChanged()
//            }
//        })
//
//        setModeSetListener(object : OnModeSetListener {
//            override fun onModeSet(id: String) {
//                if (mEntryVm.transportationName.value != null) {
//                    mEntryVm.category.value = Category(mType, id)
//                    addEntry()
//                }
//            }
//
//            override fun onModeSetWithoutChanges(id: String) {
//                mEntryVm.category.value = Category(mType, id)
//                addEntry()
//            }
//        })

//    }

//    private fun initModeTransportationView(){
//        mTransportationAdapter = ModeAdapter(mTransportationMode,mModeSelectedListener,true)
//        categoryTypeRecycler.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
//        categoryTypeRecycler.adapter = mTransportationAdapter
//    }
//    private fun initModeRecyclerView() {
//        mModeAdapter = ModeAdapter(mModeList, mModeSelectedListener,false)
//        rvAddEntryMode.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
//        rvAddEntryMode.adapter = mModeAdapter
//
//    }
//
//    private fun initModeSearchView() {
//        val filterList = arrayListOf<Mode>()
//        mModeSearchAdapter = ModeAdapter(filterList, mModeSelectedOnSearchListener,false)
//        rvAddEntryOwnMode.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
//        rvAddEntryOwnMode.adapter = mModeSearchAdapter
//    }

//    private fun filter(string: String) {
//        mModeSearchList = arrayListOf()
//        mModeList.forEach { transportationName ->
//            if (transportationName.name.toLowerCase().contains(string.toLowerCase()) && !string.isBlank()) {
//                mModeSearchList.add(transportationName)
//            }
//        }
//
//        mModeSearchAdapter.filterList(mModeSearchList)
//    }

//    private fun selectDate() {
//        val calendar = mEntryVm.startDate.value
//        if (calendar != null) {
//            setDateSelectedListener(object : OnDateSelectedListener {
//                override fun onDateSelected() {
//                    setDateText(calendar)
//                }
//            })
//            this@AddEntryActivity.showDatePickerDialog(calendar, null, mDateListener)
//        }
//    }

//    private fun selectTime() {
//        var calendar = mEntryVm.startDate.value
//        if (calendar == null) {
//            calendar = Calendar.getInstance()
//            mEntryVm.startDate.value = calendar
//        }
//        if (calendar != null) {
//            setTimeSelectedListener(object : OnTimeSelectedListener {
//                override fun onTimeSelected() {
//                    setTimeText(calendar)
//                }
//            })
//            this@AddEntryActivity.showTimePickerDialog(mEntryVm.startDate.value!!, mTimeListener)
//        }
//    }

//    private fun addEntry() {
//        if (mEntryVm.isValidEntry() && mTransportation != null) {
//            val user = GoingApplication.instance.auth.currentUser
//            if (mEntryVm.isValidEntry() && user != null) {
//                val entryId: String? = if (mEntryId != null) {
//                    mEntryId
//                } else {
//                    EntryRepository().makeEntryId()
//                }
//                val cost = mEntryVm.cost.value!!.toDouble()
//                val origin = mEntryVm.originLocation.value!!
//                val destination = mEntryVm.destinationLocation.value!!
//                val startDate = mEntryVm.startDate.value!!
//                val tag = mEntryVm.tag.value
//                val notes = mEntryVm.notes.value
//                val category = mEntryVm.category.value!!
//                val day = mEntryVm.day.value!!
//                if (category.categoryType == 0) {
//                    mEntryVm.title.value = resources.getString(
//                        R.string.transportationTitle,
//                        origin.address,
//                        destination.address,
//                        mModeName
//                    )
//                } else {
//                    mEntryVm.title.value = resources.getString(
//                        R.string.checkedIn,
//                        mModeName,
//                        destination.address
//                    )
//                }
//                val title = mEntryVm.title.value ?: ""
//                val entry = Entry(
//                    entryId,
//                    cost,
//                    Timestamp(startDate.time),
//                    null,
//                    user.uid,
//                    origin,
//                    destination,
//                    title,
//                    category,
//                    tag,
//                    notes,
//                    day,
//                    mTransportation!!.id
//                )
//                if (mEntryEditPosition == -1) {
//                    EventBus.getDefault().postSticky(mEntryList.add(entry))
//                } else {
//                    EventBus.getDefault().postSticky(mEntryList.set(mEntryEditPosition, entry))
//                }
//                goToPage(AddItineraryActivity::class.java)
//                finish()
//            }
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        } else if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        }

    }


    private fun handleOnActivityResult(resultCode: Int, data: Intent?, requestCode: Int) {
        when {
            resultCode == Activity.RESULT_OK && data != null -> {
                val place = Autocomplete.getPlaceFromIntent(data)
                if (place.latLng != null && place.address != null) {
                    if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
                        mEntryVm.originLocation.value = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.address!!
                        )
                        mEntryVm.origin.value = place.address
                    } else if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE && mType == 0) {
                        mEntryVm.destinationLocation.value = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.address!!
                        )
                        mEntryVm.destination.value = place.address
                    } else if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE && mType != 0) {
                        mEntryVm.destinationLocation.value = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.name!!
                        )
                        mEntryVm.destination.value = place.name
                    }

                }
            }
            resultCode == AutocompleteActivity.RESULT_ERROR && data != null -> {
                val snackbar = Snackbar.make(
                    addEntryRootLayout, "An unforseen error has occured. Please try again.",
                    Snackbar.LENGTH_LONG
                )
                snackbar.show()
                val status = Autocomplete.getStatusFromIntent(data)
                Log.d("errorMessage", status.statusMessage)
            }
        }
    }


    /* private fun setCategory(type: Int) {
         when (type) {
             0 -> categoryTxt.text = resources.getString(R.string.transportation)
             1 -> categoryTxt.text = resources.getString(R.string.accommodation)
             2 -> categoryTxt.text = resources.getString(R.string.spot)
         }
     }*/

//    private fun setTimeText(calendar: Calendar) {
//        val timeFormat = SimpleDateFormat("hh:mm a", Locale.US)
//        mEntryVm.startTime.value = timeFormat.format(calendar.time)
//    }

    //    private fun setDateText(calendar: Calendar) {
//        mEntryVm.startDateText.value = this@AddEntryActivity.setCalendarName(calendar)
//    }
    private fun getAddress(latitude: Double, longitude: Double) : Location {
        val geoCoder = Geocoder(this,Locale.getDefault())
        val addresses = geoCoder.getFromLocation(latitude,longitude,1)
        val address = addresses[0].getAddressLine(0)
        return Location(latitude,longitude,address)
    }

    private fun getCurrentLocation() {
        val permission: Array<String> = arrayOf(
            android.Manifest.permission.ACCESS_FINE_LOCATION
        )
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, permission,
                1
            )
        } else {
            mLocationManager.requestSingleUpdate(
                LocationManager.GPS_PROVIDER,
                mLocationListener,
                Looper.getMainLooper()
            )
        }
    }

//    private fun setEntryVmValues(event: Entry) {
//        val calendar = Calendar.getInstance()
//        ModeRepository.instance.getMode(event.type.mode, mModeListener)
//        calendar.time = event.startTime.toDate()
//        mEntryId = event.entryId
//        mEntryVm.title.value = event.title
//        mEntryVm.startDate.value = calendar
//        mEntryVm.cost.value = event.cost.toString()
//        mEntryVm.notes.value = event.notes
//        mEntryVm.origin.value = event.origin.address
//        mEntryVm.destination.value = event.destination.address
//        mEntryVm.tag.value = event.tag
//        mEntryVm.originLocation.value = event.origin
//        mEntryVm.destinationLocation.value = event.destination
//       // mEntryVm.day.value = event.day!!.toInt()
//        mEntryVm.category.value = event.type
//        mType = event.type.categoryType+1
//        mTransportationMode
//        categorySpinner.setSelection(event.type.categoryType)
//        etEntryDaySpinner.setSelection(event.day)
//    }

//    private fun setDateSelectedListener(eventListener: OnDateSelectedListener) {
//        mDateListener = eventListener
//    }
//
//    private fun setTimeSelectedListener(eventListener: OnTimeSelectedListener) {
//        mTimeListener = eventListener
//    }
//
//    private fun setModeReceivedListener(eventListener: OnModeReceivedListener) {
//        mModeListener = eventListener
//    }
//
//    private fun setModeSelectedListener(eventListener: OnModeSelectedListener) {
//        mModeSelectedListener = eventListener
//    }
//
//    private fun setModeSelectedOnSearchListener(eventListener: OnModeSelectedListener) {
//        mModeSelectedOnSearchListener = eventListener
//    }
//
//    private fun setModeSetListener(eventListener: OnModeSetListener) {
//        mModeSetListener = eventListener
//    }

//    @Subscribe(sticky = true)
//    fun onEntryListReceived(event: ArrayList<Entry>) {
//        mEntryList = event
//        val calendar = Calendar.getInstance()
//        if (mEntryEditPosition == -1) {
//            mEntryVm.originLocation.value = event[event.size - 1].destination
//            mEntryVm.origin.value = event[event.size - 1].destination.address
//            calendar.time = event[event.size - 1].startTime.toDate()
//            setTimeText(calendar)
//        } else {
//            mEntryVm.originLocation.value = mEntryList[mEntryEditPosition - 1].destination
//            mEntryVm.origin.value = mEntryList[mEntryEditPosition - 1].destination.address
//            calendar.time = event[mEntryEditPosition - 1].startTime.toDate()
//            setTimeText(calendar)
//        }
//    }

    @Subscribe(sticky = true)
    fun onEditEntryEvent(event: OnEntryReceivedEvent) {
//        setEntryVmValues(event.entry)
        mType = event.entry.type.categoryType
        mEntryEditPosition = event.position
    }

    @Subscribe(sticky = true)
    fun onAddEntryEvent(event: OnAddEntryEvent) {
        if (event.startDate != null && event.endDate != null) {
            val diff = event.endDate.timeInMillis - event.startDate.timeInMillis
            val days = diff / (1000 * 60 * 60 * 24)
            val dayArray = arrayListOf<String>()
            var count = 0
            while (count <= days) {
                val day = count + 1
                dayArray.add("Day $day")
                count++
            }
            val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dayArray)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            etEntryDaySpinner.adapter = adapter
        }
    }



    private fun arrangeListByFrequency(mode : Int) {
        if(mode == 0) {
            mModeList.sortByDescending {
                it.frequency
            }
        }
        else{
            mTransportationMode.sortByDescending {
                it.frequency
            }
        }
    }
    private fun changeFragment(fragment: Fragment) {
        val transaction = mFragmentManager.beginTransaction()
        transaction.replace(R.id.flItineary, fragment)
        transaction.commit()
    }
}
//todo shaun please check the list below.
//4.)please put error messages asap.
//5.)please try to improve the UI for entry adapter.
//thanks!
