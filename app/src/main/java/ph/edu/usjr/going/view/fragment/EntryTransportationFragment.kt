package ph.edu.usjr.going.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.Toast
import com.google.firebase.firestore.DocumentChange
import kotlinx.android.synthetic.main.fragment_entry_transportation.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.GoingApplication
import ph.edu.usjr.going.OnEntryReceivedEvent
import ph.edu.usjr.going.R
import ph.edu.usjr.going.TransportationEvent
import ph.edu.usjr.going.databinding.FragmentEntryTransportationBinding
import ph.edu.usjr.going.extensions.capitalizeAndTrim
import ph.edu.usjr.going.interfaces.OnModeSelectedListener
import ph.edu.usjr.going.interfaces.OnModeSetListener
import ph.edu.usjr.going.model.Mode
import ph.edu.usjr.going.repositories.ModeRepository
import ph.edu.usjr.going.view.adapter.ModeAdapter
import ph.edu.usjr.going.viewmodel.EntryViewModel

class EntryTransportationFragment : BaseFragment() {
    private lateinit var mEntryVm: EntryViewModel
    private lateinit var mBinding: FragmentEntryTransportationBinding
    private lateinit var mTransportationAdapter: ModeAdapter
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var mModeSetListener: OnModeSetListener
    private lateinit var mModeSelectedListener: OnModeSelectedListener
    private lateinit var mTransportationList: ArrayList<Mode>
    private var mTransportationClicked = false
    private var mEditTransportationItemPosition = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mEntryVm = ViewModelProviders.of(this).get(EntryViewModel::class.java)
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_entry_transportation,
            container,
            false
        )
        mBinding.vm = mEntryVm
        mBinding.lifecycleOwner = this
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mTransportationList = arrayListOf()
        setListeners()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        initModeTransportationView()

        checkForUpdates()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun setListeners() {
        setModeSelectedListener(object : OnModeSelectedListener {
            override fun onSelectedListener(position: Int, isTransportation: Boolean) {
                if (isTransportation) {
                    Toast.makeText(
                        context,
                        mTransportationList[position].name,
                        Toast.LENGTH_LONG
                    ).show()
                    mTransportationClicked = true
                    mEntryVm.transportation.value = mTransportationList[position]
                    mEntryVm.transportationName.value = null
                }
            }
        })

        setModeSetListener(object : OnModeSetListener {
            override fun onModeSet(mode: Mode) {
                if (mode.category == 0) {
                    mEntryVm.transportationId.value = mode.id
                    mEntryVm.transportationName.value = mode.name
                    if (mEntryVm.transportationId.value != null && mEntryVm.transportationName.value != null && activity != null && mEntryVm.isTransportationCostValid()) {
                        val tags = mEntryVm.tag.value
                        val notes = mEntryVm.notes.value
                        val transportationCost = mEntryVm.transportationCost.value!!.toDouble()
                        EventBus.getDefault().postSticky(
                            TransportationEvent(
                                mEntryVm.transportationId.value!!,
                                mEntryVm.transportationName.value!!,
                                transportationCost,
                                notes,
                                tags
                            )
                        )
                        mFragmentManager = activity!!.supportFragmentManager
                        val fragment = ViewEntryFragment()
                        val transaction = mFragmentManager.beginTransaction()
                        transaction.setCustomAnimations(
                            R.anim.enter_from_right,
                            R.anim.exit_to_left,
                            R.anim.enter_from_left,
                            R.anim.exit_to_right
                        )
                        transaction.replace(R.id.flItineary, fragment).addToBackStack(null).commit()
                    }
                }
            }
        })

        etTranspoAddMode.setOnClickListener {
            mTransportationClicked = false
            mEntryVm.transportation.value = null
        }


        entryTransportationModeNextBtn.setOnClickListener {
            if (mEntryVm.isTransportationValid()) {
                ModeRepository.instance.setMode(mModeSetListener, mEntryVm.transportation.value!!)
            } else if (!mEntryVm.transportationName.value.isNullOrBlank()) {
                mEntryVm.transportationName.value =
                    mEntryVm.transportationName.value!!.capitalizeAndTrim()
                ModeRepository.instance.setMode(
                    mModeSetListener,
                    Mode("", 0, mEntryVm.transportationName.value!!, 0)
                )
            } else {
                Toast.makeText(
                    context, "Some inputs are invalid, Please double check and try again!",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }


    /**
     * listeners
     */

    private fun setModeSelectedListener(eventListener: OnModeSelectedListener) {
        mModeSelectedListener = eventListener
    }

    private fun setModeSetListener(eventListener: OnModeSetListener) {
        mModeSetListener = eventListener
    }

    /**
     * adapter handlers
     */
    private fun initModeTransportationView() {
        mTransportationAdapter = ModeAdapter(mTransportationList, mModeSelectedListener, true)
        rvTransportationMode.layoutManager =
            LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
        rvTransportationMode.adapter = mTransportationAdapter
    }


    /**
     * database listener
     */
    private fun checkForUpdates() {
        mTransportationList.clear()
        GoingApplication.instance.database.collection("Mode")
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null) {
                    Log.d("errorMessage", "listener:error", firebaseFirestoreException)
                    return@addSnapshotListener
                }
                loop@ for (documentChange in querySnapshot!!.documentChanges) {
                    when (documentChange.type) {
                        DocumentChange.Type.ADDED -> {
                            val mode = documentChange.document.toObject(Mode::class.java)
                            if (mode.category == 0 && mTransportationList.size < 9) {
                                mTransportationList.add(mode)
                                arrangeListByFrequency()
                                mTransportationAdapter.notifyDataSetChanged()
                                if (mEntryVm.transportationId.value != null) {
                                    var counter = 0
                                    mTransportationList.forEach {
                                        if (it.id == mEntryVm.transportationId.value!!) {
                                            mEditTransportationItemPosition = counter
                                        }
                                        counter++
                                    }
                                }
                                rvTransportationMode.viewTreeObserver.addOnGlobalLayoutListener(
                                    object : ViewTreeObserver.OnGlobalLayoutListener {
                                        override fun onGlobalLayout() {
                                            if (mEditTransportationItemPosition != -1) {
                                                rvTransportationMode.findViewHolderForLayoutPosition(
                                                    mEditTransportationItemPosition
                                                )?.itemView?.performClick()
                                                rvTransportationMode.viewTreeObserver.removeOnGlobalLayoutListener(
                                                    this
                                                )
                                            }
                                        }
                                    })
                            }
                        }
                        DocumentChange.Type.MODIFIED -> {
                            val mode = documentChange.document.toObject(Mode::class.java)
                            var counter = 0
                            mTransportationList.forEach {
                                if (it.id == mode.id && mode.category == it.category) {
                                    mTransportationList[counter] = mode
                                }
                                counter++
                            }
                            arrangeListByFrequency()
                            mTransportationAdapter.notifyDataSetChanged()
                        }
                        DocumentChange.Type.REMOVED -> {
                            val mode = documentChange.document.toObject(Mode::class.java)
                            mTransportationList.remove(mode)
                            arrangeListByFrequency()
                            mTransportationAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
    }

    /**
     * list formatter
     */
    private fun arrangeListByFrequency() {
        mTransportationList.sortByDescending {
            it.frequency
        }
    }

    /**
     * Subscriber Receivers
     */

    @Subscribe(sticky = true)
    fun onEditEvent(event: OnEntryReceivedEvent) {
        mEntryVm.transportationId.value = event.entry.transportation
        mEntryVm.category.value = event.entry.type
        mEntryVm.tag.value = event.entry.tag
        mEntryVm.notes.value = event.entry.notes
        mEntryVm.transportationCost.value = event.entry.transportationCost.toString()
    }
}