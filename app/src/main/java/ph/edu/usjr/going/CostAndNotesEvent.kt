package ph.edu.usjr.going

data class CostAndNotesEvent (
    val cost : Double = 0.0,
    val notes : String? = null,
    val tags : String? = null
)