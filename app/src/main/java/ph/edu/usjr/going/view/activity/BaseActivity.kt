package ph.edu.usjr.going.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import ph.edu.usjr.going.Constants
import ph.edu.usjr.going.GoingApplication
import ph.edu.usjr.going.R


abstract class BaseActivity : AppCompatActivity() {
    val app: GoingApplication
        get() {
            return GoingApplication.instance
        }

    override fun onStart() {
        super.onStart()
        if (!app.isUserLoggedIn && !(this is LogInActivity || this is RegisterActivity)) {
            goToPage(LogInActivity::class.java)
            finish()
        }
        if (!hasAccountInDatabase()) {
            val extras = Bundle()
            extras.putString(Constants.THIRD_PARTY, getString(R.string.third_party))
            goToPage(RegisterActivity::class.java)
            finish()
        } else if (app.isUserLoggedIn && this is LogInActivity) {
            goToPage(LandingActivity::class.java)
            finish()
        }
    }

    private fun hasAccountInDatabase(): Boolean {
        var isValid = true
        this.app.database.collection("Travelers")
            .document("${app.auth.currentUser?.uid}").get()
            .addOnSuccessListener {
                if (!it.exists()) {
                    isValid = false
                }
            }
            .addOnFailureListener {
                Log.d("ErrorMessage", "${it.cause}")
            }
        return isValid
    }

    /*
    @desc used to start activity
    @params Class<out Activity> clz - the activity to be started.
    @params Bundle? extras - used if there is a need to pass extras.
     */
    fun goToPage(clz: Class<out Activity>, extras: Bundle? = null) {
        val intent = Intent(this, clz)
        //if there are extras, it will be placed here
        if (extras != null) {
            intent.putExtras(extras)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        //perform the intent
        startActivity(intent)
    }
}