package ph.edu.usjr.going

import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Journal

data class OnJournalAndEntriesEvent (
    val journal : Journal = Journal(),
    val entries : ArrayList<Entry> = arrayListOf()
)