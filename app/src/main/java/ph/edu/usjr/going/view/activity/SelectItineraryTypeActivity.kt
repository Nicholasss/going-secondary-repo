package ph.edu.usjr.going.view.activity


import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.Html
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_select_itinerary_type.*
import org.greenrobot.eventbus.EventBus
import ph.edu.usjr.going.R
import ph.edu.usjr.going.view.adapter.SliderAdapter
import java.util.logging.Logger

class SelectItineraryTypeActivity : BaseActivity() {

    private val adapter: PagerAdapter = SliderAdapter(this)
    private lateinit var mDots: Array<TextView?>
    private var mType: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_itinerary_type)
        viewPager.adapter = adapter
        addDotsIndicator(0)
        setOnClickListeners()
    }

    @Suppress("DEPRECATION")
    private fun addDotsIndicator(position: Int) {
        mDots = arrayOfNulls(2)
        dotLayout.removeAllViews()
        for (i: Int in 0 until mDots.size) {
            mDots[i] = TextView(this)
            mDots[i]?.text = Html.fromHtml("&#8226")
            mDots[i]?.textSize = 35F
            mDots[i]?.setTextColor(resources.getColor(R.color.colorAccentGrey))
            dotLayout.addView(mDots[i])
        }
        if (mDots.isNotEmpty()) {
            mDots[position]!!.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            mType = position
        }
    }

    private fun setOnClickListeners() {

        itineraryModeBackButton.setOnClickListener {
            goToPage(LandingActivity::class.java)
        }

        selectNextBtn.setOnClickListener {
            travelTypeHandler()
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {}
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
            override fun onPageSelected(i: Int) {
                addDotsIndicator(i)
                arrowButtonsConfig()
            }
        })

        arrowRight.setOnClickListener {
            viewPager.currentItem = mType!!.plus(1)
        }
        arrowLeft.setOnClickListener {
            viewPager.currentItem = mType!!.minus(1)
        }
    }

    private fun arrowButtonsConfig() {
        if (mType == 0) {
            arrowRight.isEnabled = true
            arrowLeft.isEnabled = false
            arrowRight.visibility = View.VISIBLE
            arrowLeft.visibility = View.INVISIBLE
        } else {
            arrowRight.isEnabled = false
            arrowLeft.isEnabled = true
            arrowRight.visibility = View.INVISIBLE
            arrowLeft.visibility = View.VISIBLE
        }
    }

    private fun travelTypeHandler() {
        val log = Logger.getLogger(SelectItineraryTypeActivity::class.java.name)
        log.warning("TYPE$mType")
        if (mType == 0) {
            goToPage(AddItineraryActivity::class.java)
        } else {
            goToPage(GuidedActivity::class.java)
        }
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().removeAllStickyEvents()
    }
}
