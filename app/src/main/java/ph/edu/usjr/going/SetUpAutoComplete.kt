package ph.edu.usjr.going

import android.content.Context
import android.content.Intent
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import java.util.*

class SetUpAutoComplete {
    fun buildAutoComplete(context: Context): Intent {
        val fields = Arrays.asList(
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS,
            Place.Field.NAME
        )
        return Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        ).build(context)
    }
}