package ph.edu.usjr.going.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Traveler(
    val id: String = "",
    val email: String = "",
    val firstName: String = "",
    val lastName: String = "",
    val profileUri: String? = null,
    val birthday: Long = 0,
    val baseCurrency : String? = null
) : Parcelable  
