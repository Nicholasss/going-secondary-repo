package ph.edu.usjr.going

import android.app.Application
import com.google.android.libraries.places.api.Places

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import ph.edu.usjr.going.model.UserInfo

class GoingApplication : Application() {

    companion object {
        lateinit var instance: GoingApplication
            private set
    }

    private lateinit var mAuth: FirebaseAuth
    private lateinit var mDb: FirebaseFirestore
    private lateinit var mStorage: StorageReference
    //gets an instance of mAuth.
    val auth: FirebaseAuth
        get() {
            return mAuth
        }
    //gets an instance of mDb
    val database: FirebaseFirestore
        get() {
            return mDb
        }
    //gets an instance of mStorage
    val storage: StorageReference
        get() {
            return mStorage
        }
    /*
    @desc checks the state of Firebase user.
    @return bool - true or false
     */
    val isUserLoggedIn: Boolean
        get() {
            if (auth.currentUser == null) {
                return false
            }
            return true
        }

    //gets current Firebase user.
    fun getFireBaseUserInfo(): UserInfo {
        val user = this.auth.currentUser
        if (isUserLoggedIn) {
            return UserInfo(
                user!!.uid,
                user.email.toString(),
                user.photoUrl.toString()
            )
        }
        return UserInfo()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        mAuth = FirebaseAuth.getInstance()
        mDb = FirebaseFirestore.getInstance()
        mStorage = FirebaseStorage.getInstance().getReference("/images/${Constants.FILE_NAME}")
        Places.initialize(
            applicationContext,
            "AIzaSyDsZa2mLZEgzh-7R8SRErocNodR80wrNgE"
        )
    }
}