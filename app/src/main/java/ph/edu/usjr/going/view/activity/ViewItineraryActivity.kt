package ph.edu.usjr.going.view.activity


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_view_itinerary.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.OnJournalAndEntriesEvent
import ph.edu.usjr.going.R
import ph.edu.usjr.going.databinding.ActivityViewItineraryBinding
import ph.edu.usjr.going.extensions.setCalendarName
import ph.edu.usjr.going.interfaces.OnEntriesReceivedListener
import ph.edu.usjr.going.interfaces.OnItineraryReceivedListener
import ph.edu.usjr.going.interfaces.OnJournalCreatedListener
import ph.edu.usjr.going.interfaces.OnJournalReceivedListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Itinerary
import ph.edu.usjr.going.model.Journal
import ph.edu.usjr.going.repositories.EntryRepository
import ph.edu.usjr.going.repositories.ItineraryRepository
import ph.edu.usjr.going.repositories.JournalRepository
import ph.edu.usjr.going.view.adapter.EntryAdapter
import ph.edu.usjr.going.viewmodel.ItineraryViewModel
import java.util.*
import kotlin.collections.ArrayList

class ViewItineraryActivity : BaseActivity() {
    private lateinit var mItineraryVm: ItineraryViewModel
    private lateinit var mAdapter: EntryAdapter
    private lateinit var mItinerary: Itinerary
    private lateinit var mEntriesReceiverListener: OnEntriesReceivedListener
    private lateinit var mItineraryReceivedListener: OnItineraryReceivedListener
    private lateinit var mJournalCreatedListener: OnJournalCreatedListener
    private lateinit var mJournalReceivedListener: OnJournalReceivedListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_itinerary)
        mItineraryVm = ViewModelProviders.of(this).get(ItineraryViewModel::class.java)
        DataBindingUtil

            .setContentView<ActivityViewItineraryBinding>(
                this,
                R.layout.activity_view_itinerary
            )
            .apply {
                this.lifecycleOwner = this@ViewItineraryActivity
                this.vm = mItineraryVm
            }
        setUpListeners()
    }


    private fun setUpListeners() {
        viewItineraryMenuButton.setOnClickListener {
            val popupMenu = PopupMenu(this, it)
            popupMenu.setOnMenuItemClickListener { view ->
                when (view.itemId) {
                    R.id.viewItEditBtn -> {
                        EventBus.getDefault().postSticky(mItinerary)
                        EventBus.getDefault().postSticky(mItineraryVm.entriesCollection.value)
                        goToPage(AddItineraryActivity::class.java)
                        Log.d("Edit", "Edit ni")
                        true
                    }

                    R.id.viewItDeleteBtn -> {
                        if (mItineraryVm.id.value != null && mItineraryVm.isEntriesListValid()) {
                            ItineraryRepository().deleteItinerary(mItineraryVm.id.value!!)
                            goToPage(LandingActivity::class.java)
                            Log.d("Delete", "Delete ni")
                            finish()
                        }
                        true
                    }
                    else -> false
                }
            }
            popupMenu.inflate(R.menu.itinerary_options)
            popupMenu.show()
        }

        itineraryViewBackButton.setOnClickListener {
            finish()
        }

        viewItStartTravelBtn.setOnClickListener {
            JournalRepository.instance.getJournal(mJournalReceivedListener)
        }

        view_progress.visibility = View.VISIBLE
        onEntriesReceivedListener(object : OnEntriesReceivedListener {
            override fun onEntriesReceived(entries: ArrayList<Entry>) {
                mItineraryVm.entriesCollection.value = entries
                if (mItineraryVm.entriesCollection.value != null) {
                    mItineraryVm.entriesCollection.value!!.sortBy {
                        it.startTime
                    }
                    initRecyclerView()
                    if(::mAdapter.isInitialized) {
                        mAdapter.notifyDataSetChanged()
                    }
                    view_progress.visibility = View.GONE
                }
            }
        })

        onItineraryReceivedListener(object : OnItineraryReceivedListener {
            override fun onItinerariesReceived(itineraryList: ArrayList<Itinerary>) {
            }

            override fun onItineraryReceived(itinerary: Itinerary) {
                setSupportActionBar(toolbar)
                setItineraryVm(itinerary)
                viewItineraryBar.title = itinerary.title
            }
        })

        onJournalCreatedListener(object : OnJournalCreatedListener {
            override fun onJournalCreated(journal: Journal) {
                EventBus.getDefault().postSticky(
                    OnJournalAndEntriesEvent(
                        journal,
                        mItineraryVm.entriesCollection.value!!
                    )
                )
                goToPage(StartJournalActivity::class.java)
            }
        })

        onJournalReceivedListener(object : OnJournalReceivedListener {
            override fun onJournalReceived(journal: Journal?) {
                if (journal == null) {
                    if (mItineraryVm.isEntriesListValid()) {
                        val entries = arrayListOf<Entry>()
                        mItineraryVm.entriesCollection.value!!.forEach {
                            val entry = Entry(
                                it.entryId,
                                it.transportationCost,
                                it.activityCost,
                                it.startTime,
                                it.finishedDate,
                                it.creatorId,
                                it.origin,
                                it.destination,
                                it.title,
                                it.type,
                                it.tag,
                                it.notes,
                                it.day,
                                it.transportation,
                                it.nights,
                                false
                            )
                            entries.add(entry)
                        }
                        JournalRepository.instance.makeJournal(
                            mItinerary,
                            entries,
                            mJournalCreatedListener
                        )
                    }
                } else {
                    Toast.makeText(
                        this@ViewItineraryActivity,
                        "You already have an existing journal. finish your journal first!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun initRecyclerView() {
        if (mItineraryVm.isEntriesListValid()) {
            mAdapter =
                EntryAdapter(
                    mItineraryVm.entriesCollection.value!!,
                    this@ViewItineraryActivity
                )
            rvViewIt.layoutManager =
                LinearLayoutManager(this, LinearLayout.VERTICAL, false)
            rvViewIt.adapter = mAdapter
        }
    }


    @Subscribe(sticky = true)
    fun onItineraryReceived(event: Itinerary) {
        setItineraryVm(event)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        ItineraryRepository().getItinerary(mItinerary.id, mItineraryReceivedListener)
        EntryRepository().getEntries(mItinerary.id, mEntriesReceiverListener, "Itineraries")
    }


    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }


    private fun onEntriesReceivedListener(eventListener: OnEntriesReceivedListener) {
        mEntriesReceiverListener = eventListener
    }

    private fun onItineraryReceivedListener(eventListener: OnItineraryReceivedListener) {
        mItineraryReceivedListener = eventListener
    }

    private fun onJournalCreatedListener(eventListener: OnJournalCreatedListener) {
        mJournalCreatedListener = eventListener
    }

    private fun onJournalReceivedListener(eventListener: OnJournalReceivedListener) {
        mJournalReceivedListener = eventListener
    }

    private fun setItineraryVm(itinerary: Itinerary) {
        mItinerary = itinerary
        val calendar = Calendar.getInstance()
        calendar.time = itinerary.startDate.toDate()
        mItineraryVm.id.value = itinerary.id
        mItineraryVm.totalCost.value = itinerary.totalCost.toString()
        mItineraryVm.travelName.value = itinerary.title
        mItineraryVm.travelerCount.value = itinerary.travelerCount.toString()
        mItineraryVm.origin.value = itinerary.origin.address
        mItineraryVm.originLoc.value = itinerary.origin
        mItineraryVm.destination.value = itinerary.destination.address
        mItineraryVm.destinationLoc.value = itinerary.destination
        mItineraryVm.startDateCalendar.value = calendar
        if (mItineraryVm.startDateCalendar.value != null) {
            mItineraryVm.startDate.value =
                this@ViewItineraryActivity.setCalendarName(mItineraryVm.startDateCalendar.value!!)
        }
        mItineraryVm.endDateCalendar.value = calendar
        mItineraryVm.endDateCalendar.value?.time = itinerary.endDate.toDate()
        if (mItineraryVm.endDateCalendar.value != null) {
            mItineraryVm.endDate.value =
                this@ViewItineraryActivity.setCalendarName(mItineraryVm.endDateCalendar.value!!)
        }
    }
}
