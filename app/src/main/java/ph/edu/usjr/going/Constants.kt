package ph.edu.usjr.going

import java.util.*

class Constants {
    companion object {
        var FILE_NAME = UUID.randomUUID().toString()
//        const val KEY_USER_ID = "intent_key_user_id"
//        const val KEY_USER_INFO = "intent_key_user_info"
        const val KEY_CATEGORY_TYPE = "intent_key_category_type"
        const val KEY_ENTRY_LISTENER = "intent_key_entry_listener"
        const val THIRD_PARTY = "intent_third_party"
        const val PERMISSION_GALLERY = 10
        const val GALLERY_REQUEST = 1111
        const val GOOGLE_SIGN_IN = 111
        const val FACEBOOK_SIGN_IN = 12312
        const val AUTOCOMPLETE_ORIGIN_REQUEST_CODE = 69
        const val AUTOCOMPLETE_DESTINATION_REQUEST_CODE = 70
        const val BASE_URL = "http://192.168.254.111:5000"
    }
}