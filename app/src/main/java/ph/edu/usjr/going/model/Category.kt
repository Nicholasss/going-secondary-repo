package ph.edu.usjr.going.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    val categoryType: Int = 0,
    val mode : String = ""
):Parcelable