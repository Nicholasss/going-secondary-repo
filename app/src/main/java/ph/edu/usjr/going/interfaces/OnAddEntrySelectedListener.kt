package ph.edu.usjr.going.interfaces

import android.os.Parcelable
import ph.edu.usjr.going.model.Entry

interface OnAddEntrySelectedListener : Parcelable{
    fun addEntrySelected(entry : Entry)
}