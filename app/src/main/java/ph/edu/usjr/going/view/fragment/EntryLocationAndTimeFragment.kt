package ph.edu.usjr.going.view.fragment

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.location.Geocoder
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentChange
import kotlinx.android.synthetic.main.fragment_entry_location_time.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ph.edu.usjr.going.*
import ph.edu.usjr.going.databinding.FragmentEntryLocationTimeBinding
import ph.edu.usjr.going.extensions.capitalizeAndTrim
import ph.edu.usjr.going.extensions.showTimePickerDialog
import ph.edu.usjr.going.interfaces.OnModeSelectedListener
import ph.edu.usjr.going.interfaces.OnModeSetListener
import ph.edu.usjr.going.interfaces.OnTimeSelectedListener
import ph.edu.usjr.going.model.Category
import ph.edu.usjr.going.model.Location
import ph.edu.usjr.going.model.Mode
import ph.edu.usjr.going.repositories.ModeRepository
import ph.edu.usjr.going.view.adapter.ModeAdapter
import ph.edu.usjr.going.viewmodel.EntryViewModel
import java.text.SimpleDateFormat
import java.util.*

class EntryLocationAndTimeFragment : BaseFragment() {
    private lateinit var mEntryVm: EntryViewModel
    private lateinit var mBinding: FragmentEntryLocationTimeBinding
    private lateinit var mModeAdapter: ModeAdapter
    private lateinit var mModeSelectedListener: OnModeSelectedListener
    private lateinit var mModeSetListener: OnModeSetListener
    private lateinit var mModeList: ArrayList<Mode>
    private var mModeClicked = false
    private var mEditModeItemPosition = -1
    private var mType = 1
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var mTimeListener: OnTimeSelectedListener
    private lateinit var mLocationListener: LocationListener

    private var mEntryEditPosition: Int = -1
    private var requestMode = 0

    private lateinit var mLocationManager: LocationManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mEntryVm = ViewModelProviders.of(this).get(EntryViewModel::class.java)
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_entry_location_time,
            container,
            false
        )
        mLocationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mBinding.vm = mEntryVm
        mBinding.lifecycleOwner = this
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mModeList = arrayListOf()
        setListeners()

        etDestination.setOnClickListener {
            if (context != null) {
                val intent = SetUpAutoComplete().buildAutoComplete(context!!)
                startActivityForResult(intent, Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE)
            }
        }

        etEntryStartTime.setOnClickListener {
            selectTime()
        }


        destinationCurrentLocBtn.setOnClickListener {
            getCurrentLocation()
        }

        entryLocTimeNxtBtn.setOnClickListener {
            sendModeToDatabase()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        initModeActivityView()
        checkForUpdates()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun setListeners() {
        etEntryDaySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mEntryVm.day.value = position.toString()
            }
        }

        mLocationListener = object : LocationListener {
            override fun onLocationChanged(location: android.location.Location?) {
                if (requestMode == 0 && location != null) {
                    mEntryVm.originLocation.value =
                        getAddress(location.latitude, location.longitude)
                    mEntryVm.origin.value = mEntryVm.originLocation.value!!.address
                }
            }

            override fun onProviderDisabled(provider: String?) {
                startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }

            override fun onProviderEnabled(provider: String?) {
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }
        }

        setModeSelectedListener(object : OnModeSelectedListener {
            override fun onSelectedListener(position: Int, isTransportation: Boolean) {
                if (!isTransportation) {
                    Toast.makeText(
                        context,
                        mModeList[position].name,
                        Toast.LENGTH_LONG
                    ).show()
                    mModeClicked = true
                    mEntryVm.mode.value = mModeList[position]
                    mEntryVm.modeName.value = null
                }
            }
        })

        setModeSetListener(object : OnModeSetListener {
            override fun onModeSet(mode: Mode) {
                if (mode.category == 1 || mode.category == 2) {
                    mEntryVm.category.value = Category(mType, mode.id)
                    if (mEntryVm.isOriginValid() && mEntryVm.isValidDay() && mEntryVm.isDestinationValid() && activity != null && mEntryVm.isValidStartDate() && mEntryVm.category.value != null && mEntryVm.isActivityCostValid()) {
                        val origin = mEntryVm.originLocation.value!!
                        val destination = mEntryVm.destinationLocation.value!!
                        val day = mEntryVm.day.value!!.toInt()
                        val time = mEntryVm.startDate.value!!
                        val cost = mEntryVm.activityCost.value!!.toDouble()
                        val nights = mEntryVm.nights.value?.toIntOrNull()
                        if ((mType == 1 && nights != null) || mType != 1) {
                            EventBus.getDefault().postSticky(
                                LocationTimeEvent(
                                    origin, destination, day,
                                    Timestamp(time.time),
                                    mEntryVm.category.value!!,
                                    mode.name,
                                    cost,
                                    nights
                                )
                            )
                            mFragmentManager = activity!!.supportFragmentManager
                            val fragment = EntryTransportationFragment()
                            val transaction = mFragmentManager.beginTransaction()
                            transaction.setCustomAnimations(
                                R.anim.enter_from_right,
                                R.anim.exit_to_left,
                                R.anim.enter_from_left,
                                R.anim.exit_to_right
                            )
                            transaction.replace(R.id.flItineary, fragment).addToBackStack(null)
                                .commit()
                        }
                    } else {
                        Toast.makeText(
                            context, "Some inputs are invalid, Please double check and try again!",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        })

        etCategoryAddMode.setOnClickListener {
            mModeClicked = false
            mEntryVm.mode.value = null
        }
        entryCategorySpinner.setSelection(0)
        entryCategorySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mType = position + 1
                if (mType == 1) {
                    etCostPerNight.visibility = View.VISIBLE
                } else {
                    etCostPerNight.visibility = View.INVISIBLE
                }
                checkForUpdates()
            }
        }
    }

    private fun setTimeText(calendar: Calendar) {
        val timeFormat = SimpleDateFormat("hh:mm a", Locale.US)
        mEntryVm.startTime.value = timeFormat.format(calendar.time)
    }

    private fun selectTime() {
        var calendar = mEntryVm.startDate.value
        if (calendar == null) {
            calendar = Calendar.getInstance()
            mEntryVm.startDate.value = calendar
        }
        if (calendar != null) {
            setTimeSelectedListener(object : OnTimeSelectedListener {
                override fun onTimeSelected() {
                    setTimeText(calendar)
                }
            })
            context?.showTimePickerDialog(mEntryVm.startDate.value!!, mTimeListener)
        }
    }

    private fun sendModeToDatabase() {
        if (mEntryVm.isModeValid()) {
            ModeRepository.instance.setMode(mModeSetListener, mEntryVm.mode.value!!)

        } else if (!mEntryVm.modeName.value.isNullOrBlank()) {
            mEntryVm.modeName.value =
                mEntryVm.modeName.value!!.capitalizeAndTrim()
            ModeRepository.instance.setMode(
                mModeSetListener,
                Mode("", mType, mEntryVm.modeName.value!!, 0)
            )
        }
    }

    private fun initModeActivityView() {
        mModeAdapter = ModeAdapter(mModeList, mModeSelectedListener, false)
        rvCategoryMode.layoutManager =
            LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
        rvCategoryMode.adapter = mModeAdapter
    }

    /**
     * result handling
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        } else if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
            handleOnActivityResult(resultCode, data, requestCode)
        }

    }


    private fun handleOnActivityResult(resultCode: Int, data: Intent?, requestCode: Int) {
        when {
            resultCode == Activity.RESULT_OK && data != null -> {
                val place = Autocomplete.getPlaceFromIntent(data)
                if (place.latLng != null && place.address != null) {
                    if (requestCode == Constants.AUTOCOMPLETE_ORIGIN_REQUEST_CODE) {
                        mEntryVm.originLocation.value = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.address!!
                        )
                        mEntryVm.origin.value = place.address
                    } else if (requestCode == Constants.AUTOCOMPLETE_DESTINATION_REQUEST_CODE) {
                        mEntryVm.destinationLocation.value = Location(
                            place.latLng!!.latitude,
                            place.latLng!!.longitude,
                            place.name!!
                        )
                        mEntryVm.destination.value = place.name
                    }

                }
            }
            resultCode == AutocompleteActivity.RESULT_ERROR && data != null -> {
                val snackBar = Snackbar.make(
                    fragmentEntryLocationTimeLayout,
                    "An unforeseen error has occurred. Please try again.",
                    Snackbar.LENGTH_LONG
                )
                snackBar.show()
                val status = Autocomplete.getStatusFromIntent(data)
                Log.d("errorMessage", status.statusMessage)
            }
        }
    }

    /**
     * listeners
     */

    private fun setTimeSelectedListener(eventListener: OnTimeSelectedListener) {
        mTimeListener = eventListener
    }

    private fun setModeSelectedListener(eventListener: OnModeSelectedListener) {
        mModeSelectedListener = eventListener
    }

    private fun setModeSetListener(eventListener: OnModeSetListener) {
        mModeSetListener = eventListener
    }

    /**
     * EventBus
     */

    @Subscribe(sticky = true)
    fun receiveOnAddEvent(event: OnAddEntryEvent) {
        if (event.entries != null) {
            if (mEntryEditPosition == -1 && event.entries.size == 0) {
                mEntryVm.origin.value = event.origin?.address
                mEntryVm.originLocation.value = event.origin
            } else if (mEntryEditPosition == -1 && event.entries.isNotEmpty()) {
                mEntryVm.originLocation.value = event.entries[event.entries.size - 1].destination
                mEntryVm.origin.value = event.entries[event.entries.size - 1].destination.address
            }
        }
    }

    @Subscribe(sticky = true)
    fun onEditEntryEvent(event: OnEntryReceivedEvent) {
        mEntryVm.origin.value = event.entry.origin.address
        mEntryVm.originLocation.value = event.entry.origin
        mEntryVm.destination.value = event.entry.destination.address
        mEntryVm.destinationLocation.value = event.entry.destination
        val calendar = Calendar.getInstance()
        calendar.time = event.entry.startTime.toDate()
        mEntryVm.startDate.value = calendar
        setTimeText(calendar)
        etEntryDaySpinner.setSelection(event.entry.day)
        mEntryEditPosition = event.position
        mEntryVm.activityCost.value = event.entry.activityCost.toString()
        mEntryVm.nights.value = event.entry.nights.toString()
        mEntryVm.category.value = event.entry.type
        entryCategorySpinner.setSelection(event.entry.type.categoryType - 1)
    }

    /**
     * Location handlers
     */
    private fun getAddress(latitude: Double, longitude: Double): Location {
        val geoCoder = Geocoder(context, Locale.getDefault())
        val addresses = geoCoder.getFromLocation(latitude, longitude, 1)
        val address = addresses[0].getAddressLine(0)
        return Location(latitude, longitude, address)
    }

    private fun getCurrentLocation() {
        if (context != null) {
            val permission: Array<String> = arrayOf(
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(permission, 1)
            } else {
                mLocationManager.requestSingleUpdate(
                    LocationManager.GPS_PROVIDER,
                    mLocationListener,
                    Looper.getMainLooper()
                )
            }
        }
    }

    /**
     * DatabaseListener
     */
    private fun checkForUpdates() {
        mModeList.clear()
        GoingApplication.instance.database.collection("Mode")
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null) {
                    Log.d("errorMessage", "listener:error", firebaseFirestoreException)
                    return@addSnapshotListener
                }
                for (documentChange in querySnapshot!!.documentChanges) {
                    when (documentChange.type) {
                        DocumentChange.Type.ADDED -> {
                            val mode = documentChange.document.toObject(Mode::class.java)
                            if (mode.category == mType && mModeList.size < 9 && mode.category != 0) {
                                mModeList.add(mode)
                                arrangeListByFrequency()
                                mModeAdapter.notifyDataSetChanged()
                                if (mEntryVm.category.value != null) {
                                    var counter = 0
                                    mModeList.forEach {
                                        if (it.id == mEntryVm.category.value!!.mode) {
                                            mEditModeItemPosition = counter
                                        }
                                        counter++
                                    }
                                }
                                rvCategoryMode.viewTreeObserver.addOnGlobalLayoutListener(object :
                                    ViewTreeObserver.OnGlobalLayoutListener {
                                    override fun onGlobalLayout() {
                                        if (mEditModeItemPosition != -1) {
                                            rvCategoryMode.findViewHolderForLayoutPosition(
                                                mEditModeItemPosition
                                            )?.itemView?.performClick()
                                            rvCategoryMode.viewTreeObserver.removeOnGlobalLayoutListener(
                                                this
                                            )
                                        }
                                    }
                                })
                            }
                        }
                        DocumentChange.Type.MODIFIED -> {
                            val mode = documentChange.document.toObject(Mode::class.java)
                            var counter = 0
                            mModeList.forEach {
                                if (it.id == mode.id && mode.category == it.category) {
                                    mModeList[counter] = mode
                                }
                                counter++
                            }
                            arrangeListByFrequency()
                            mModeAdapter.notifyDataSetChanged()
                        }
                        DocumentChange.Type.REMOVED -> {
                            val mode = documentChange.document.toObject(Mode::class.java)
                            mModeList.remove(mode)
                            arrangeListByFrequency()
                            mModeAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
    }

    private fun arrangeListByFrequency() {
        mModeList.sortByDescending {
            it.frequency
        }
    }
}