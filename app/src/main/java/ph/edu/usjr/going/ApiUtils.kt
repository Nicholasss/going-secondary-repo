package ph.edu.usjr.going

import ph.edu.usjr.going.Constants.Companion.BASE_URL
import ph.edu.usjr.going.interfaces.JournalPlaceHolderApi

class ApiUtils {
    fun getAPIService() :JournalPlaceHolderApi {
        return RetrofitClient().getClient()!!.create(JournalPlaceHolderApi::class.java)
    }
}