package ph.edu.usjr.going.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Bindable
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Location
import java.lang.NumberFormatException
import java.util.*

class ItineraryViewModel : ViewModel() {
    val id = MutableLiveData<String>()
    @Bindable
    val totalCost = MutableLiveData<String>()

    init {
        totalCost.value = "0"
    }

    @Bindable
    val travelName = MutableLiveData<String>()
    @Bindable
    val travelerCount = MutableLiveData<String>()
    @Bindable
    val origin = MutableLiveData<String>()
    @Bindable
    val destination = MutableLiveData<String>()
    @Bindable
    val startDate = MutableLiveData<String>()
    @Bindable
    val endDate = MutableLiveData<String>()
    val startDateCalendar = MutableLiveData<Calendar>()
    val endDateCalendar = MutableLiveData<Calendar>()
    val destinationLoc = MutableLiveData<Location>()
    val originLoc = MutableLiveData<Location>()
    val entriesCollection = MutableLiveData<ArrayList<Entry>>()
    val days = MutableLiveData<Long>()

    init {
        entriesCollection.value = arrayListOf()
    }

    private fun isTravelNameValid(): Boolean {
        val isValid = true
        val travelName = travelName.value
        if (travelName.isNullOrBlank()) {
            return false
        }
        return isValid
    }

    private fun isTravelNumberValid(): Boolean {
        val isValid = true
        val travelNumber = travelerCount.value
        if (travelNumber.isNullOrBlank()) {
            return false
        }
        try {
            if (travelNumber.toDouble() > 99) {
                return false
            }
        } catch (e: NumberFormatException) {
            return false
        }
        return isValid
    }

    private fun isOriginValid(): Boolean {
        val isValid = true
        val originText = origin.value
        val origin = originLoc.value
        if (originText.isNullOrBlank()) {
            return false
        }
        if (origin == null) {
            return false
        }
        return isValid
    }

    private fun isDestinationValid(): Boolean {
        val isValid = true
        val destinationText = destination.value
        val destination = destinationLoc.value
        if (destinationText.isNullOrBlank()) {
            return false
        }
        if (destination == null) {
            return false
        }
        return isValid
    }

    fun isEntriesListValid(): Boolean {
        val isValid = true
        val list = entriesCollection.value ?: return false
        if (list.isEmpty()) {
            return false
        }
        return isValid
    }

    fun isValidStartDate(): Boolean {
        val isValid = true
        val startDate = startDate.value
        val startDateCalendar = startDateCalendar.value
        val entries = entriesCollection.value
        if (startDate.isNullOrBlank()) {
            return false
        }
        if (startDateCalendar == null) {
            return false
        }

        return isValid
    }

    fun isValidEndDate(): Boolean {
        val isValid = true
        val endDate = endDate.value
        val startDateCalendar = startDateCalendar.value ?: return false
        val endDateCalendar = endDateCalendar.value
        val entries = entriesCollection.value
        if (endDate.isNullOrBlank()) {
            return false
        }
        if (endDateCalendar == null) {
            return false
        }
        if(startDateCalendar>endDateCalendar){
            return false
        }

        return isValid
    }

    private fun isTotalCostValid(): Boolean {
        val isValid = true
        val totalCost = totalCost.value
        if (totalCost.isNullOrBlank()) {
            return false
        }
        try {
            totalCost.toDouble()
        } catch (e: NumberFormatException) {
            return false
        }
        return isValid
    }

//    fun setStartDateAndEndDate() {
//        val entries = entriesCollection.value
//        startDateCalendar.value = null
//        endDateCalendar.value = null
//        if (isEntriesListValid()) {
//            entries!!.forEach {
//                if (startDateCalendar.value == null) {
//                    startDateCalendar.value = Calendar.getInstance()
//                    startDateCalendar.value?.timeInMillis = it.startTime.toDate().time
//                }
//                if (endDateCalendar.value == null) {
//                    endDateCalendar.value = Calendar.getInstance()
//                    endDateCalendar.value?.timeInMillis = it.startTime.toDate().time
//                }
//                if (it.startTime.toDate().time < startDateCalendar.value!!.timeInMillis) {
//                    startDateCalendar.value!!.timeInMillis = it.startTime.toDate().time
//                }
//                if (it.startTime.toDate().time > endDateCalendar.value!!.timeInMillis) {
//                    endDateCalendar.value!!.timeInMillis = it.startTime.toDate().time
//                }
//            }
//        } else {
//            startTime.value = ""
//            endDate.value = ""
//        }
//    }

    fun addToTotalCost(): Double {
        val entries = entriesCollection.value
        var sum = 0.0
        if (isEntriesListValid()) {
            entries!!.forEach {
                sum += it.transportationCost + it.activityCost
            }
        }
        return sum
    }

    fun subtractTotalCostFromEntryPosition(position: Int): Boolean {
        val isValid = true
        val cost = totalCost.value
        val entries = entriesCollection.value
        if (!isEntriesListValid()) {
            return false
        }
        if (entries!!.size <= position) {
            return false
        }
        if (isTotalCostValid()) {
            val difference = cost!!.toDouble() - (entries[position].activityCost + entries[position].transportationCost)
            totalCost.value = difference.toString()
        }
        return isValid
    }

    fun isItineraryValid(): Boolean {
        val isValid = true

        if (!isDestinationValid()) {
            return false
        }
        if (!isOriginValid()) {
            return false
        }
        if (!isTravelNameValid()) {
            return false
        }
        if (!isTravelNumberValid()) {
            return false
        }
        if (!isValidStartDate()) {
            return false
        }
        if (!isValidEndDate()) {
            return false
        }
        if (!isTotalCostValid()) {
            return false
        }
        return isValid
    }

    fun inputValues(
        id: String? = null,
        title: String? = null,
        travelCount: Int? = null,
        originLoc: Location? = null,
        destinationLoc: Location? = null,
        startDate: Calendar? = null,
        endDate: Calendar? = null
    ) {
        this.id.value = id
        travelName.value = title
        this.originLoc.value = originLoc
        this.destinationLoc.value = destinationLoc
        origin.value = originLoc?.address
        destination.value = destinationLoc?.address
        if (travelCount != null) {
            travelerCount.value = travelCount.toString()
        }
        if (startDate != null) {
            this.startDateCalendar.value = startDate
        }
        if (endDate != null) {
            this.endDateCalendar.value = endDate
        }
    }
    fun setDays(){
        if (endDateCalendar.value != null && startDateCalendar.value != null) {
            val diff = endDateCalendar.value!!.timeInMillis - startDateCalendar.value!!.timeInMillis
            val day = diff / (1000 * 60 * 60 * 24)
            days.value = day
        }
    }
}