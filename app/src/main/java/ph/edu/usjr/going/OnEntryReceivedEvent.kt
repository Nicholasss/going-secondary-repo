package ph.edu.usjr.going

import ph.edu.usjr.going.model.Entry

data class OnEntryReceivedEvent(
    val entry: Entry = Entry(),
    val position: Int = -1
)
