package ph.edu.usjr.going

import com.google.firebase.Timestamp
import ph.edu.usjr.going.model.Category
import ph.edu.usjr.going.model.Location

data class LocationTimeEvent(
    val origin : Location,
    val destination : Location,
    val days : Int = -1,
    val startTime : Timestamp = Timestamp(0,0),
    val category : Category,
    val modeName : String,
    val activityCost : Double = 0.0,
    val nights : Int? = null
)