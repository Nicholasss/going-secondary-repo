package ph.edu.usjr.going.view.activity

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.View
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_log_in.*
import ph.edu.usjr.going.Constants
import ph.edu.usjr.going.R
import ph.edu.usjr.going.databinding.ActivityLogInBinding
import ph.edu.usjr.going.viewmodel.LogInViewModel
import java.util.*

class LogInActivity : BaseActivity() {

    private lateinit var mLogInVM: LogInViewModel
    private lateinit var mCallbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        mCallbackManager = CallbackManager.Factory.create()
        mLogInVM = ViewModelProviders.of(this).get(LogInViewModel::class.java)
        DataBindingUtil
            .setContentView<ActivityLogInBinding>(this, R.layout.activity_log_in)
            .apply {
                this.lifecycleOwner = this@LogInActivity
                this.logInVm = mLogInVM
            }
    }

    //OnClick method for Log In(Firebase)
    fun logInClick(view: View) {

        if (mLogInVM.isValidLogIn()) {
            invalidErr.visibility = View.INVISIBLE
            progress_landing.visibility = View.VISIBLE
            progress_bg.visibility = View.VISIBLE
            logInBtn.visibility = View.INVISIBLE
            signInWithEmailAndPassword("${mLogInVM.email.value}", "${mLogInVM.password.value}")
        } else {
            progress_landing.visibility = View.INVISIBLE
            progress_bg.visibility = View.INVISIBLE
            pwdErr.visibility = View.INVISIBLE
            invalidErr.visibility = View.VISIBLE
            logInBtn.visibility = View.VISIBLE
        }
    }

    //OnClick method for Log In(Google)
    fun googleLogClick(view: View) {
        //create a sign in client for google
        val gso = buildGso()
        startActivityForResult(gso.signInIntent, Constants.GOOGLE_SIGN_IN)
    }

    //OnClick method for Log In(Facebook)
    fun facebookLogClick(view: View) {

        //get the following required read permissions for Facebook.
        mLogInVM.logInManager.logInWithReadPermissions(
            this,
            Arrays.asList("public_profile", "user_friends")
        )
        //request a Facebook token
        mLogInVM.logInManager.registerCallback(mCallbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onCancel() {
                }

                override fun onError(error: FacebookException?) {
                }

                override fun onSuccess(result: LoginResult?) {
                    if (result != null) {
                        //if the result is successful, provide the access token.
                        exchangeFacebookToken(result.accessToken)
                    }
                }
            })
    }

    //OnClick method to intent to Register.
    fun registerClick(view: View) {
        goToPage(RegisterActivity::class.java)
    }

    private fun failedLogInDisplay() {
        progress_landing.visibility = View.INVISIBLE
        progress_bg.visibility = View.INVISIBLE
        pwdErr.visibility = View.VISIBLE
        logInBtn.visibility = View.VISIBLE
    }

    /**
    @desc handles results from startOnActivityResults
    @params Int requestCode - check kind of request code provided.
    @params Int resultCode - check if the result was ok or not.
    @params Intent? data - data result from intent
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            //handle Google result
            requestCode == Constants.GOOGLE_SIGN_IN -> {
                //get the result of task
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    if (account != null)
                    //handle account
                        handleGoogleLogIn(account)
                } catch (e: ApiException) {
                }
            }
            //handle Facebook result
            resultCode == Activity.RESULT_OK -> {
                mCallbackManager.onActivityResult(requestCode, resultCode, data)
            }
            else -> {
                //todo error message here again shaun
            }
        }
    }

    // function that handles the Facebook token.
    fun exchangeFacebookToken(token: AccessToken) {
        val user = this.app.auth.currentUser
        val credentials = FacebookAuthProvider.getCredential(token.token)
        //Sign in with Facebook credentials.
        this.app.auth.signInWithCredential(credentials)
            .addOnCompleteListener(this) {
                this.app.database
                    .collection("Travelers")
                    .document("${user?.uid}").get()
                    .addOnSuccessListener {
                        val extras = Bundle()
                        val clz: Class<out Activity> = if (it.data != null) {
                            LandingActivity::class.java
                        } else {
                            RegisterActivity::class.java
                        }
                        extras.putString(Constants.THIRD_PARTY, getString(R.string.third_party))
                        goToPage(clz, extras)
                    }
                    .addOnFailureListener {
                        Log.e("errorMessage", "${it.message}")
                    }
            }
    }

    /*
    @desc function that handles google log in
    @params GoogleSignInAccount account - an account from onActivityResult.
     */
    private fun handleGoogleLogIn(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        //Log in with Google credentials.
        this.app.auth
            .signInWithCredential(credential)
            .addOnCompleteListener {
                this.app.database
                    .collection("Travelers")
                    .document(this.app.auth.currentUser!!.uid).get()
                    .addOnSuccessListener {
                        val extras = Bundle()

                        //if account exists, go to the main page, otherwise, go to register.
                        if (it.data != null) {
                            goToPage(LandingActivity::class.java)
                        } else {
                            extras.putString(Constants.THIRD_PARTY, getString(R.string.third_party))
                            goToPage(RegisterActivity::class.java, extras)
                        }
                    }
                    //if failed to sign in with credentials
                    .addOnFailureListener {
                        failedLogInDisplay()
                        Log.e("errorMessage", "${it.cause}")
                    }
            }
    }

    private fun signInWithEmailAndPassword(email: String, password: String) {
        this.app.auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                val extras = Bundle()
                pwdErr.visibility = View.INVISIBLE
                goToPage(LandingActivity::class.java, extras)
            }
            .addOnFailureListener {
                failedLogInDisplay()
            }
    }

    private fun buildGso(): GoogleSignInClient {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            // TODO: Ahdz, This should be hidden. Find a way to hide this sensitive information
            .requestIdToken("409178318310-2sv1q4fbfh5u5uk4kchktg5rco84glmb.apps.googleusercontent.com")
            .requestEmail()
            .build()
        return GoogleSignIn.getClient(this, gso)
    }
}