package ph.edu.usjr.going.view.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import ph.edu.usjr.going.R
import ph.edu.usjr.going.view.fragment.RegisterFragment
import ph.edu.usjr.going.view.fragment.RegisterThirdPartyFragment

class RegisterActivity : BaseActivity() {
    private lateinit var mFragmentManager: FragmentManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mFragmentManager = supportFragmentManager
        //if there are any extras, go to third party register, if there isn't, go to register fragment
        if (intent != null && intent.extras != null) {
            changeFragment(RegisterThirdPartyFragment())
        } else {
            changeFragment(RegisterFragment())
        }
    }

    private fun changeFragment(fragment: Fragment) {
        val transaction = mFragmentManager.beginTransaction()
        transaction.replace(R.id.flRegister, fragment)
        transaction.commit()
    }
}