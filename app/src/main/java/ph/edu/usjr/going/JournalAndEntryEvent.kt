package ph.edu.usjr.going

data class JournalAndEntryEvent (
    val journalId : String = "",
    val entryId : String = ""
)