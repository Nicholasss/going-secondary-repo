package ph.edu.usjr.going.viewmodel


import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class SelectCategoryViewModel : ViewModel() {
    val mode = MutableLiveData<Int>()
    val categoryType = MutableLiveData<Int>()

    init {
        categoryType.value = 0
    }

    fun isCategoryTypeValid(): Boolean {
        val isValid = true
        val categoryType = categoryType.value ?: return false
        if (categoryType != 1 && categoryType != 0) {
            return false
        }
        return isValid
    }

    private fun isModeValid(): Boolean {
        val isValid = true
        val mode = mode.value ?: return false
        if (mode < 0 || mode > 8) {
            return false
        }
        return isValid
    }

    fun isCategoryValid(): Boolean {
        val isValid = true
        if (!isCategoryTypeValid()) {
            return false
        }
        if (!isModeValid()) {
            return false
        }
        return isValid
    }
}