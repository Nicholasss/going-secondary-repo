package ph.edu.usjr.going.repositories

import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseUser
import ph.edu.usjr.going.GoingApplication
import ph.edu.usjr.going.interfaces.OnItineraryReceivedListener
import ph.edu.usjr.going.interfaces.OnSetEntryCompleteListener
import ph.edu.usjr.going.model.Entry
import ph.edu.usjr.going.model.Itinerary
import ph.edu.usjr.going.model.Location
import java.util.*


class ItineraryRepository {
    private lateinit var mItinerary: Itinerary
    fun setItinerary(
        user: FirebaseUser,
        title: String,
        travelerCount: Int,
        origin: Location,
        destination: Location,
        totalCost: Double,
        startDate: Calendar,
        endDate: Calendar,
        published: Boolean,
        itineraryId: String? = null,
        days : Long,
        listener : OnItineraryReceivedListener
    ) {
        if (itineraryId == null) {
            val ref = GoingApplication.instance.database.collection("Itineraries").document()
            val id = ref.id
            mItinerary =
                Itinerary(
                    user.uid, travelerCount, origin, destination, totalCost, title, id,
                    Timestamp(startDate.time),
                    Timestamp(endDate.time)
                    , published,days
                )
//            EntryRepository().setEntries(entries, id,null,"Itineraries")
            ref.set(mItinerary)
            listener.onItineraryReceived(mItinerary)
        } else {
            GoingApplication.instance.database.collection("Itineraries").document(itineraryId)
            mItinerary =
                Itinerary(
                    user.uid, travelerCount, origin, destination, totalCost, title, itineraryId,
                    Timestamp(startDate.time),
                    Timestamp(endDate.time)
                    , published,days
                )
            GoingApplication.instance.database.collection("Itineraries").document(itineraryId)
                .set(mItinerary)
            listener.onItineraryReceived(mItinerary)
//            EntryRepository().deleteEntries(itineraryId, entries, listener,"Itineraries")
        }

    }

    fun getItineraries(user: FirebaseUser, listener: OnItineraryReceivedListener) {
        val itineraryList: ArrayList<Itinerary> = arrayListOf()
        GoingApplication.instance.database.collection("Itineraries").get()
            .addOnSuccessListener {
                for (index in it) {
                    val itinerary = index.toObject(Itinerary::class.java)
                    if (itinerary.creatorId == user.uid && !itinerary.published) {
                        itineraryList.add(itinerary)
                    }
                }
                listener.onItinerariesReceived(itineraryList)
            }
    }

    fun getItinerary(itineraryId: String, listener: OnItineraryReceivedListener) {
        GoingApplication.instance.database.collection("Itineraries").document(itineraryId).get()
            .addOnSuccessListener {
                val itinerary = it.toObject(Itinerary::class.java)
                if (itinerary != null) {
                    listener.onItineraryReceived(itinerary)
                }
            }
    }

    fun deleteItinerary(itineraryId: String) {
        GoingApplication.instance.database.collection("Itineraries").document(itineraryId)
            .collection("entries").document().delete()
        GoingApplication.instance.database.collection("Itineraries").document(itineraryId).delete()
    }
}